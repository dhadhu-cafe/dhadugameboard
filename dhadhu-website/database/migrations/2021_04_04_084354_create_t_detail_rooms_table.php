<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTDetailRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('t_games_id');
            $table->integer('t_players_id');
            $table->integer('room_master_id');
            $table->integer('max_players');
            $table->text('description_room');
            $table->string('t_rooms_id');
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_rooms');
    }
}
