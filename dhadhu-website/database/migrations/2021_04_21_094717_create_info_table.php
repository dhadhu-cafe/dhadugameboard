<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->id();
            $table->string('kontak', 20);
            $table->string('Alamat', 255);
            $table->string('Senin', 191);
            $table->string('Selasa', 191);
            $table->string('Rabu', 191);
            $table->string('Kamis', 191);
            $table->string('Jumat', 191);
            $table->string('Sabtu', 191);
            $table->string('Minggu', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
