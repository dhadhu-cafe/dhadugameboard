<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTFnbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnbs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_fnb', 191);
            $table->string('type_fnb', 191);
            $table->integer('price_fnb');
            $table->text('description_fnb');
            $table->string('image_fnb', 191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fnbs');
    }
}
