<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('player_name', 191);
            $table->string('fname', 191);
            $table->string('lname', 191);
            $table->string('email', 191);
            $table->string('password', 191);
            $table->string('cfmPassword', 191);
            $table->string('gender', 191);
            $table->string('player_image', 191)->nullable();
            $table->integer('exp');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
