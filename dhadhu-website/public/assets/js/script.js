// Ketika user klik tombol roller
function coba() {
    document.getElementById("myDropdown").classList.toggle("show");
}
//highlight sidenav
var header = document.getElementById("myDiv");
var btns = header.getElementsByClassName("nav-link");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("nyala");
  current[0].className = current[0].className.replace(" nyala", "");
  this.className += " nyala";
  });
}

// dropdown PP
function AvaPP() {
    document.getElementById("ProfPP").classList.toggle("show");
}
window.onclick = function(event) {
    if (!event.target.matches('img')) {
      var dropdowns = document.getElementsByClassName("dropdown-option");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
            }
        }
    }
}

//pindah menu in sidenav
var ListNav = document.getElementsByClassName("nav-link");
var SectionCount = document.getElementsByClassName("mysection");

let idxMenu = 0;
let idxSection = 0;

for (idxMenu = 0; idxMenu < ListNav.length; idxMenu++) {
    (function(myIndexMenu) {
        ListNav[myIndexMenu].addEventListener("click", ()=>{
            for (idxSection = 0; idxSection < SectionCount.length; idxSection++) {
                SectionCount[idxSection].classList.remove("active");
            }
            SectionCount[myIndexMenu].classList.toggle("active");

        });
    }(idxMenu));
}


//menu in roller - play
var PlayList = document.getElementsByClassName("nav-play");
var DivCount = document.getElementsByClassName("play-tab");

let idxplay = 0;
let idxdiv = 0;

for (idxplay = 0; idxplay < PlayList.length; idxplay++) {
    (function(myPlaymenu) {
        PlayList[myPlaymenu].addEventListener("click", ()=>{
            for (idxdiv = 0; idxdiv < DivCount.length; idxdiv++) {
                DivCount[idxdiv].style.display = "none";
                DivCount[idxdiv].style.opacity = "0";
            }
            DivCount[myPlaymenu].style.display = "block";
            setTimeout(() => {
                DivCount[myPlaymenu].style.opacity = "1";
            }, 50);
            
        });
    }(idxplay));
}

// hightlight play
var headPlay = document.getElementById("DivPlay");
var btnPlay = headPlay.getElementsByClassName("nav-play");
for (var i = 0; i < btnPlay.length; i++) {
    console.log(i);
  btnPlay[i].addEventListener("click", function() {
  var currentx = document.getElementsByClassName("turnOn");
  currentx[0].className = currentx[0].className.replace(" turnOn", "");
  this.className += " turnOn";
  });
}

//menu in roller - leaderboard
var PlayListLead = document.getElementsByClassName("nav-play-lead");
var DivCounts = document.getElementsByClassName("play-leaderboard");

let idxplaylead = 0;
let idxdivlead = 0;

for (idxplaylead = 0; idxplaylead < PlayListLead.length; idxplaylead++) {
    (function(myPlaymenu) {
        PlayListLead[myPlaymenu].addEventListener("click", ()=>{
            for (idxdivlead = 0; idxdivlead < DivCounts.length; idxdivlead++) {
                DivCounts[idxdivlead].style.display = "none";
                DivCounts[idxdivlead].style.opacity = "1";
            }
            DivCounts[myPlaymenu].style.display = "block";
            setTimeout(() => {
                DivCounts[myPlaymenu].style.opacity = "1";
            }, 50);
            
        });
    }(idxplaylead));
}
//highlight leaderoboard
var headLight = document.getElementById("myDivLead");
var btnLight = headLight.getElementsByClassName("nav-play-lead");
for (var i = 0; i < btnLight.length; i++) {
  btnLight[i].addEventListener("click", function() {
  var currentx = document.getElementsByClassName("hidup");
  currentx[0].className = currentx[0].className.replace(" hidup", "");
  this.className += " hidup";
  });
}

// mapbox map scritp
mapboxgl.accessToken = 'pk.eyJ1IjoibGFlZGRpcyIsImEiOiJja2xraTdndHUyczZsMndtbHB4NXloYzU1In0.I2q4T1TbUHd3iOVyXXXLjw';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [110.44691745644948, -7.060694939995662], // starting position as [lng, lat]], 
    zoom: 13
});
//pop-up
var popup = new mapboxgl.Popup()
.setHTML('<h3>Dhadhu Gameboard Cafe</h3>');
//marker
var marker = new mapboxgl.Marker()
.setLngLat([110.44691745644948, -7.060694939995662])
.setPopup(popup)
.addTo(map);

//show/hide sidenav
var btnControl = document.getElementById("btnMenu");
var navLeft = document.getElementById("SideLeft");
var navRight = document.getElementById("SideRight");


btnControl.addEventListener("click", function(){
    navLeft.classList.toggle("active");
    navRight.classList.toggle("active");
})

//filter leaderboard
function filterLeaderboard(){
    var filterLeaderboard = document.getElementById("FilterLeaderboardGame").value;
    console.log(filterLeaderboard);
    alert(filterLeaderboard);
    document.getElementById("loading_image").style.display = "block";
    $.ajax({
      type: "GET",
      url: "/dashboard/filter",
      data:{
        "filterLeaderboard" : filterLeaderboard,
      },
      success:function(data){
          
          $('.ldTime').html(data['htmlLdTime'])
          $('.ldScore').html(data['htmlLd'])
          $('.ldHighScore').html(data['htmlHighScore'])
        },
        complete: function(){
          document.getElementById("loading_image").style.display = "none";
        }
    })
}

//filter events
function filterEvents(){
    var filterEvents = document.getElementById("filterEvents").value;
    document.getElementById("loading_image").style.display = "block";
    $.ajax({
      type: "GET",
      url: "/dashboard/filterEvents",
      data:{
        "filterEvents" : filterEvents,
      },
      success:function(data){
          $('#eventsRender').html(data['htmlFilterEvents'])
          console.log(data)
        },
        complete: function(){
          document.getElementById("loading_image").style.display = "none";
        }
    })
}

//modals
function show(id) {
    var modal = document.getElementById("eventsModal-".concat(id));
    
    modal.style.display = "block";

    window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }   
    }
}

function showFNB(id) {
    var fnb = document.getElementById("fnbModal-".concat(id));

    fnb.style.display = "block";

    window.onclick = function(event) {
        if (event.target == fnb) {
            fnb.style.display = "none";
        }
    }
}

function showPnM(id) {
    var pnm = document.getElementById("pnmModal-".concat(id));
    
    pnm.style.display = "block";

    window.onclick = function(event) {
        if (event.target == pnm) {
            pnm.style.display = "none";
        }   
    }
}

function showGame(id) {
    var sGame = document.getElementById("gameModal-".concat(id));

    sGame.style.display = "block";

    window.onclick = function(event) {
        if (event.target == sGame) {
            sGame.style.display = "none";
        }   
    }
}

function showGamex(id) {
    var G = document.getElementById("gxModal-".concat(id));

    G.style.display = "block";

    window.onclick = function(event) {
        if (event.target == G) {
            G.style.display = "none";
        }   
    }
}

function FilterShow() {
    var FS = document.getElementById("filterPromo");

    FS.style.display = "block";

    window.onclick = function(event) {
        if (event.target == FS) {
            FS.style.display = "none"
        }
    }
}

function FilterShowGame() {
    var FSG = document.getElementById("filterGame");

    FSG.style.display = "block";

    window.onclick = function(event) {
        if (event.target == FSG) {
            FSG.style.display = "none"
        }
    }
}

var filter          = document.getElementById("ShowFilter");
var BtnFilterClose  = document.getElementById("filterBatal");
var CloseFilter     = document.getElementById("filterShow");

filter.addEventListener("click", function(){
    CloseFilter.classList.toggle("active");
})
BtnFilterClose.addEventListener("click", function(){
    CloseFilter.classList.toggle("active");
})

var filterGame      = document.getElementById("showfilter");
var BtnFilter       = document.getElementById("BTNclose");
var Closebtn        = document.getElementById("FILTERShow")

filterGame.addEventListener("click", function(){
    Closebtn.classList.toggle("active");
    // alert("test")
})
BtnFilter.addEventListener("click", function(){
    Closebtn.classList.toggle("active");
})

//
function multipleFilters(){
      var selectedMonth = $("input[name='selectRow[]']:checked").map(function(){
      return $(this).val();
      }).get();      
      var selectedEvent = $("input[name='selectRowEvent[]']:checked").map(function(){
      return $(this).val();
      }).get();

      var searchData = $("input[name='searchEvents']").map(function(){
        return $(this).val();
        }).get();

        console.log(searchData)
      document.getElementById("loading_image").style.display = "block";
      $.ajax({
        type                : "GET",
        url                 : "/dashboard/multiFilterEvent",
        data:{
          '_method'         : 'GET',
          'months'          : selectedMonth,
          'events'          : selectedEvent,
          'search'          : searchData,
        },
        success: function(data){
          $('#eventsRender').html(data['htmlFilterEvents'])
        },
        complete: function(){
          document.getElementById("loading_image").style.display = "none";
        }
      })
  }

function multipleFiltersGame(){

    var searchData = $("input[name='searchGames']").map(function(){
        return $(this).val();
        }).get();

    var selectRowDifficulty = $("input[name='selectRowDifficulty']:checked").map(function(){
        return $(this).val();
        }).get();
    var selectRowMaxp = $("input[name='selectRowMaxp']:checked").map(function(){
        return $(this).val();
        }).get();
    var selectRowDur = $("input[name='selectRowDur']:checked").map(function(){
        return $(this).val();
        }).get();
    var selectRowAge = $("input[name='selectRowAge']:checked").map(function(){
        return $(this).val();
        }).get();

    console.log(searchData)
    console.log(selectRowDifficulty)
    console.log(selectRowMaxp)
    console.log(selectRowDur)
    console.log(selectRowAge)

    document.getElementById("loading_image").style.display = "block";
    $.ajax({
        type                : "GET",
        url                 : "/dashboard/multiFilterGame",
        data:{
        '_method'         : 'GET',
        // '_token'          : csrfToken,   
        'search'                : searchData,
        'selectRowDifficulty'   : selectRowDifficulty,
        'selectRowMaxp'         : selectRowMaxp,
        'selectRowDur'          : selectRowDur,
        'selectRowAge'          : selectRowAge,
        },

        success: function(data){
        $('#componentFilterGame').html(data['htmlFilterGames'])
        },
        complete: function(){
        document.getElementById("loading_image").style.display = "none";
        }
    })
}
