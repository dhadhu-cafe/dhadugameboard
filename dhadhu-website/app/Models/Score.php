<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Score extends Model
{
    use HasFactory;
    use softDeletes;
    
    public function game()
    {
        return $this->hasMany(Game::class); 
    }

    public function player()
    {
        return $this->hasMany(Player::class); 
    }
}
