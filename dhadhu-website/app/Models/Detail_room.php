<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail_room extends Model
{
    use HasFactory;
    
    public function room()
    {
        return $this->belongsTo('room', 'id');
    }
    
    public function game()
    {
        return $this->hasMany(Game::class); 
    }

    public function player()
    {
        return $this->hasMany(Player::class); 
    }
}
