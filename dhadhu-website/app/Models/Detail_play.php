<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail_play extends Model
{
    use HasFactory;
    public function game()
    {
        return $this->hasMany(Game::class); 
    }

    public function player()
    {
        return $this->hasMany(Player::class); 
    }
}
