<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    public function score()
    {
        return $this->belongsTo(Score::class);
    }
    
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function detail_room()
    {
        return $this->belongsTo(detail_room::class);
    }

    public function detail_play()
    {
        return $this->belongsTo(detail_play::class);
    }

    
}
