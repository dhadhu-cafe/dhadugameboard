<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    public function game()
    {
        return $this->hasMany(Game::class); 
    }

    public function player()
    {
        return $this->hasMany(Player::class); 
    }

    protected $table = 'rooms';
 
    public function detail_room()
    {
        return $this->hasMany('detail_room', 't_rooms_id'); 
    }
}
