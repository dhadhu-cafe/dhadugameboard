<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Challenges_Record extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $table = 'challenges_records';

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }
}
