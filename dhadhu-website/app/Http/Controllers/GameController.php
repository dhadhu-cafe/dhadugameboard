<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\Categories;
use App\Models\Media;
use File;
Use \Carbon\Carbon;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class GameController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $games = Game::select("id", "game_name", "category_game",
                                "age", "jml_player", "durasi",
                                "difficulty_game","image_game","description_game")->paginate(10);
        $categoriesGames = categories::where('category_type','Game')->get();
                                // dd($games);
        return view('pages/page_game', compact('games', 'categoriesGames', 'admin'));
    }

    public function store(Request $request)
    {
        $gameName = $request->gameName;
        $image = $request->imageGame;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $image->move(public_path()."/gambar", $timeOriginalName);
        $categoryGame = $request->categoryGame;
        $difficultyGame = $request->difficulty;
        if($difficultyGame == 1){
            $difficultyGame = "Easy";
        }else if($difficultyGame == 2){
            $difficultyGame = "Normal";
        }else if($difficultyGame == 3){
            $difficultyGame = "Hardcore";
        }
        $newGame = new Game;
        $newGame->game_name         = $request->gameName;
        $newGame->category_game     = $categoryGame;
        $newGame->age               = $request->age;
        $newGame->jml_player        = $request->jmlplayer;
        $newGame->durasi            = $request->duration;
        $newGame->difficulty_game   = $difficultyGame;
        $newGame->description_game  = $request->descGame;
        $newGame->image_game        = $timeOriginalName;
        $newGame->save();
        $newImage = new Media;
        $newImage->original_name = $timeOriginalName;
        $newImage->image_name = $gameName;
        $newImage->image_category = $categoryGame;
        $newImage->save();

        return redirect()->back()->with('success', 'berhasil Input Data!');
    }

    public function destroy(Request $request)
    {
        $id = $request->deleteGameId;
        // dd($id);
        $selectedData = Game::where('id', $id)->first();
        $imageName = $selectedData->image_game;
        $oldDestinationPath = public_path().'/gambar/'.$imageName;
        $newDestinationPath = public_path().'/delete/'.$imageName;
        $move = File::move($oldDestinationPath, $newDestinationPath);
        if($move){
            $deleteFile = Game::where('id',$id)->delete();
            if($deleteFile){
                return [
                    'result' => 'success',
                    'message' => 'Berhasil menghapus data!',
                ];
            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat menghapus data!',
                ];
            }
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus gambar!',
            ];
        }
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $id = Crypt::decryptString($id);
        $prevImage = $request->prevImage;
        $oldDestinationPath = public_path().'/gambar/'.$prevImage;
        $newDestinationPath = public_path().'/delete/'.$prevImage;
        if($oldDestinationPath == true){
            $move = File::move($oldDestinationPath, $newDestinationPath);
            // dd($move);
        }else{
            return [
                'result' => 'error',
                'message' => 'Kesalahan saat mengedit gambar!',
            ];
        }

        $image = $request->imageGame;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $editGame = Game::find($id);
        $editGame->game_name            = $request->gameName;
        $editGame->category_game        = $request->categoryGame;
        $editGame->age                  = $request->age;
        $editGame->jml_player           = $request->jmlPlayer;
        $editGame->durasi               = $request->durasi;
        $editGame->difficulty_game      = $request->difficultyGame;
        $editGame->image_game           = $timeOriginalName;
        $editGame->description_game     = $request->descGame;
        $editGame->save();

        return redirect()->back()->with('success', 'berhasil edit Data Game!');
    }
}
