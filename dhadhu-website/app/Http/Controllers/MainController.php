<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fnb;
use App\Models\Game;
use App\Models\Event;
use App\Models\Challenge;
use App\Models\Challenges_record;
use App\Models\Player;
use App\Models\Score;
use App\Models\Room;
use App\Models\Detail_room;
use App\Models\Detail_play;
use App\Models\Categories;
use App\Models\INfo;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;

class MainController extends Controller
{
    public function menu(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        // dd($id);
        $categoryMenus = Categories::where('category_type', 'Menu Fnb')->get();
        $menus = Fnb::select('id', 'name_fnb', 'type_fnb',
                            'price_fnb', 'description_fnb', 'image_fnb')
                            ->get();

        //BEGIN::ROLLER game
        $games = Game::select('id','game_name', 'category_game',	'age',
                                'jml_player', 'durasi', 'total_play', 
                                'difficulty_game', 'image_game', 
                                'description_game')
                                ->get();
                               

        $topGames = Game::orderBy('total_play', 'desc')->paginate(3);

        $favGame = Detail_play::join('games', 'games.id', '=', 'detail_plays.t_games_id')
                        ->join('players', 'players.id', '=', 'detail_plays.t_players_id')
                        ->select('games.game_name', 'games.category_game', 'games.image_game', 
                                 'games.difficulty_game', 'detail_plays.played')
                        ->where("t_players_id", $id)
                        ->orderBy("played", 'desc')
                        ->first();
        
        $playeds = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                        ->join('players', 'players.id', '=', 'scores.t_players_id')
                        ->select('games.game_name', 'games.category_game', 'games.image_game')
                        ->where("t_players_id", $id)
                        ->get();

        $recentPlay = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                            ->join('players', 'players.id', '=', 'scores.t_players_id')
                            ->select('games.game_name', 'games.category_game',	'games.age',
                                'games.jml_player', 'games.durasi', 'games.total_play', 
                                'games.difficulty_game', 'games.image_game', 
                                'games.description_game')
                            ->orderBy('scores.created_at', 'desc')
                            ->first();
        // dd($recentPlay);
        // $countPlays = Score::where()
        //END::ROLLER game

        // BEGIN::PROMOS AND MORE
         $eventsDashboard = Event::select('id', 'event_name', 'category',
                                          'image_event', 'description_event')
                                 ->orderBy('id', 'desc')
                                 ->paginate(3);
         $events = Event::select('id', 'event_name', 'category', 'image_event', 'description_event')->get();
        // END::PROMOS AND MORE
        
        //BEGIN::CHALLANGE
        $challenges = Challenge::select('id', 'challenge_name', 'description_challenge', 'exp')->get();
        // dd($challenges);
        $statusChallenges = Challenges_record::join('challenges', 'challenges.id', '=', 'Challenges_records.challenges_id')
                                                ->join('players', 'players.id', '=', 'Challenges_records.t_players_id')
                                                ->where('players.id', '=', $id)
                                                ->select('challenges.id', 'challenges.challenge_name','challenges.description_challenge', 
                                                         'challenges_records.challenges_id', 'challenges_records.status', 'challenges_records.t_players_id')
                                                ->get();
        
        // dd($statusChallenges);
        //END::CHALLANGE

        //BEGIN::PLAYER
        $players = Player::all();
        $user = Player::where('id', '=', $id)->first();
        //END::PLAYER

        //BEGIN::SCORE

        $filterLeaderboard='1';
        $leaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('total_score', 'desc')
                                ->paginate(10);
                                // dd($leaderboards);
        $highScoreLeaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('score', 'desc')
                                ->paginate(10);
        // dd($highScoreLeaderboards);
        $leaderboardsTime = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('playtime', 'desc')
                                ->paginate(10);
                                
        //END::SCORE

        // BEGIN::ROOM
        $rooms = Room::select('id', 'room_name', 't_games_id',
                        't_players_id', 'max_players', 'description_room', 'date')
                        ->get();
        $joinedRooms = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                ->where('detail_rooms.t_players_id', $id)
                                ->select('rooms.id', 'rooms.room_name', 'games.game_name', 
                                         'rooms.description_room', 'rooms.max_players', 'rooms.date',
                                         'detail_rooms.t_players_id')
                                ->get();
        // dd($joinedRooms);
        // $joinedRoomAsMember = Detail_room::
        $detailRoomAsMasters = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('room_master_id', $id)
                                    ->get();

        $controllRoom = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('room_master_id', $id)
                                    ->select('rooms.id', 'rooms.room_name', 'games.game_name', 'rooms.description_room', 'rooms.max_players', 'rooms.date',)
                                    ->first();

        $controllRoomForMember = Detail_room::where('detail_rooms.t_players_id', $id)
                                    ->join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->select('rooms.id', 'rooms.room_name', 'games.game_name', 'rooms.description_room', 
                                            'rooms.max_players', 'rooms.date', 'detail_rooms.t_players_id')
                                    ->first();
        // dd($controllRoomForMember);
        $detailRoomAsMember = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('detail_rooms.t_players_id', $id)
                                    ->first();
                                    
        if($detailRoomAsMember != null){
            $roomId = $detailRoomAsMember->t_rooms_id;
            $rm = $detailRoomAsMember->room_master_id;
            $dataMembers = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                        ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                        ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                        ->where('detail_rooms.t_rooms_id', $roomId)
                                        ->where('detail_rooms.room_master_id', $rm)
                                        ->select('players.id', 'players.player_name', 'players.exp', 'status')
                                        ->orderBy('detail_rooms.id')
                                        ->get();
            // dd($dataMembers);  
        }else{
            $dataMembers=null;
        }
        
        
        $roomMaster = Detail_room::where('room_master_id', '=', $id)->first();
        $joinRoom = Detail_room::where('t_players_id', '=', $id)->first();
        $newRooms = Room::select('rooms.id', 'rooms.room_name', 'rooms.description_room', 'rooms.t_games_id', 'games.game_name', 'rooms.max_players')
                        // ->join('detail_rooms', 'detail_rooms.id', '=', 'rooms.t_detail_rooms_id')
                        ->join('games', 'games.id', '=', 'rooms.t_games_id')
                        ->join('players', 'players.id', '=', 'rooms.t_players_id')
                        ->get();
                        // dd($newRooms);
        // END::ROOM

        //BEGIN::categories filter
        $categoryGames = Categories::where('category_type', 'Game')->get();
        $categoryEvents = Categories::where('category_type', 'Postingan')->get();
        $categoryFnbs = Categories::where('category_type', 'Menu FnB')->get();
        //END::categories filter
        //info 
        $info = info:: select('kontak','Alamat','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu')->get();
        return view('welcome_user', compact('categoryMenus', 'menus', 'games', 'topGames', 
                                            'events', 'challenges', 'players', 
                                            'user', 'leaderboards', 'leaderboardsTime',
                                            'rooms', 'newRooms', 'statusChallenges',
                                            'detailRoomAsMasters', 'roomMaster', 'joinRoom',
                                            'dataMembers', 'controllRoom', 'controllRoomForMember',
                                            'categoryGames', 'categoryEvents', 'categoryFnbs',
                                            'playeds', 'eventsDashboard', 'joinedRooms',
                                            'recentPlay', 'info', 'highScoreLeaderboards',
                                            'favGame'));
    }

    public function filter(Request $request)
    {
        // dd($request->all());
        $filterLeaderboard = $request->filterLeaderboard;

        $game = Game::where('game_name', $filterLeaderboard)->first();
        // dd($game);
        if($game == null){
            $idGame = '0';
        }else{
            $idGame = $game->id;
        }

        $leaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $idGame)
                                ->orderBy('total_score', 'desc')
                                ->paginate(10);
                                
        $highScoreLeaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $idGame)
                                ->orderBy('score', 'desc')
                                ->paginate(10);
        // dd($highScoreLeaderboards);
        $leaderboardsTime = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $idGame)
                                ->orderBy('playtime', 'desc')
                                ->paginate(10);
        // dd($leaderboardsTime);
        
        $filterLd = view('components/leaderboardScore', compact('leaderboards'))->render();
        $filterLdHighScore = view('components/leaderboardHighScore', compact('highScoreLeaderboards'))->render();
        $filterLdTime = view('components/leaderboardTime', compact('leaderboardsTime'))->render();
        // dd($filterLd);
        return response()->json(['htmlLd' => $filterLd, 'htmlLdTime' => $filterLdTime, 'htmlHighScore'=>$filterLdHighScore]);
    }

    public function filterEvents(Request $request)
    {
        // dd($request->all());
        $filterMonths = $request->months;
        $filterEvents = $request->events;
        $search = $request->search;

        if($search!="" && $filterMonths!="" && $filterEvents!=""){
            $events = Event::where('event_name', '=', $search)
                            ->where('category', $filterEvents)
                            ->whereMonth('created_at', $filterMonths)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();

                return response()->json(['htmlFilterEvents' => $filtered]);
            }
        }else if($search!="" && $filterMonths!=""){
            $events = Event::where('event_name', '=', $search)
                    ->whereMonth('created_at', $filterMonths)
                    ->get();
            if($events){
            $filtered = view('components/filterEvents', compact('events'))->render();

            return response()->json(['htmlFilterEvents' => $filtered]);
            }
        }else if($search!="" && $filterEvents!=""){
            $events = Event::where('event_name', '=', $search)
                            ->where('category', $filterEvents)
                            ->get();
            if($events){
            $filtered = view('components/filterEvents', compact('events'))->render();

            return response()->json(['htmlFilterEvents' => $filtered]);
            }
        }else if($search!=""){
            $events = Event::where('event_name', '=', $search)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();

               return response()->json(['htmlFilterEvents' => $filtered]);
            }
        }else if($filterMonths!="" && $filterEvents!=""){
            $events = Event::where('category', $filterEvents)
                            ->whereMonth('created_at', $filterMonths)
                            ->get();

            if($events){
               $filtered = view('components/filterEvents', compact('events'))->render();

               return response()->json(['htmlFilterEvents' => $filtered]);
            }
        }else if($filterMonths!=""){
            $events = Event::whereMonth('created_at', $filterMonths)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();
    
                return response()->json(['htmlFilterEvents' => $filtered]);
                }
        }else if($filterEvents!=""){
            $events = Event::where('category', $filterEvents)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();
    
                return response()->json(['htmlFilterEvents' => $filtered]);
                }
        }      
    }

    public function createRoom(Request $request, $id)
    {
        // dd($request->all());
        $roomName = $request->roomName;
        $maxPlayers = $request->maxPLayer;
        $gameName = $request->gameName;    
        $gameData = Game::where('game_name', $gameName)->first();
        if($gameData == null){
            return redirect()->back()->with('warning', 'Error saat membuat room. *Game '.$gameName.' tidak ada!');
        }
        $maxPlayersInGame = $gameData->jml_player;
        $playDate = $request->date;
        $today = date('Y-m-d');
        if($maxPlayers <= 0){
            return redirect()->back()->with('warning', 'Error saat membuat room. *Jumlah Player tidak boleh kurang dari 0');
        } else if($maxPlayers > $maxPlayersInGame){
            return redirect()->back()->with('warning', 'Error saat membuat room. *'.$gameName.' hanya bisa dimainkan sebanyak '.$maxPlayersInGame.' player.  (MAX)');
        }
        if($playDate <= $today){
            return redirect()->back()->with('warning', 'Error saat membuat room '.$roomName.' *Tanggal Harus 1 hari lebih dari tanggal saat ini!');
        }
        $gameId = $gameData->id;
        $player = Player::where('id', '=', $id)->first();
        $playerId = $player->id;
        $playerName = $player->player_name;
        $createRoom = new Room;
        $createRoom->room_name = $roomName;
        $createRoom->t_games_id = $gameId;
        $createRoom->t_players_id = $playerId;
        $createRoom->max_players = $maxPlayers;
        $createRoom->date = $request->date;
        $createRoom->description_room = $request->descriptionRoomGame;
        $createRoom->save();
        
        $detailRoomData = room::get('id')->last();
        $FirstCustomDetailRoomId = Str::beforeLast($detailRoomData, '}');
        $detailRoomId = Str::afterLast($FirstCustomDetailRoomId, ':');
        $roomMaster = new Detail_room;
        $roomMaster->t_games_id         = $gameId;
        $roomMaster->t_players_id       = $playerId;
        $roomMaster->room_master_id       = $playerId;
        $roomMaster->max_players        = $maxPlayers;
        $roomMaster->description_room   = $request->descriptionRoomGame;
        $roomMaster->t_rooms_id         = $detailRoomId;
        $roomMaster->status             = 'joined';
        // dd($roomMaster);
        $roomMaster->save();
        return redirect()->back()->with('success', $roomName.' has been created!');

    }

    public function joinRoom(Request $request, $id)
    {
        // dd($request->all());
        // dd($id);
        $roomId = $request->roomId;
        $cekInRoom = detail_room::where('t_players_id', $id)
                                ->where('t_rooms_id', $roomId)
                                ->get();
        // dd($cekInRoom);
        if(count($cekInRoom)){
            return redirect()->back()->with('warning', 'you already in room!');
        }
        $dataRoom = detail_room::where('t_rooms_id', '=', $roomId)->first();
        $status = detail_room::where('status', '=', 'joined')->count();
        $maxPlayers = $request->maxPlayers;
        // dd($status);
        if($status <= $maxPlayers){
            $status = 'joined';
        }else{
            $status = 'waiting';
        }
        $roomMaster = $dataRoom->room_master_id;
        $joinPlayer = new Detail_room;
        $joinPlayer->t_games_id         = $request->gameId;
        $joinPlayer->t_players_id       = $id;
        $joinPlayer->room_master_id     = $roomMaster;
        $joinPlayer->max_players        = $maxPlayers;
        $joinPlayer->description_room   = $request->dscRoom;
        $joinPlayer->t_rooms_id         = $roomId;
        $joinPlayer->status             = $status;
        $joinPlayer->save();
        return redirect()->back()->with('success', 'Success join into '.$request->roomName);
    }

    public function destroyRoom(Request $request, $id)
    {
        // dd($request->all());
        $allPlayers = Detail_room::where('t_rooms_id', '=', $id)->delete();
        $dataRoom = Room::where('id', '=', $id)->delete();
        return redirect()->back()->with('success', 'Success delete room');
    }

    public function leaveRoom(Request $request, $id)
    {

        $detailRoomId = $request->roomId;
        // dd($detailRoomId);
        
        $leaveRoom = Detail_room::where('t_rooms_id', $detailRoomId)
                                ->where('t_players_id', $id)
                                ->delete();
        if($leaveRoom){
            $jml_players = Detail_room::where('t_rooms_id', $detailRoomId)->count();
            $dataRoom = Room::where('id', '=', $detailRoomId)->first();
            $maxPlayers = $dataRoom->max_players;
            if($jml_players >= $maxPlayers){
                $dataPlayer = Detail_room::where('status', 'waiting')
                                            ->orderBy('id')
                                            ->first();
                $dataPlayer->status = 'joined';
                $dataPlayer->save();
            }
        }
        return redirect()->back()->with('success', 'Success leave room');
    }
    
    public function cekRoom(Request $request, $id)
    {
        $roomId = $request->roomId;
              
        $roomMasterCek = Detail_room::where('room_master_id', $id)
                                 ->where('t_rooms_id', $roomId)
                                 ->get();
                                //  dd($roomMasterCek);
        if(count($roomMasterCek)){
            $roomMaster = Detail_room::where('room_master_id', '=', $id)
                                ->where('t_players_id', '=', $id)
                                ->where('t_rooms_id', '=', $roomId)
                                ->first();
            $detailRoomAsMasters = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('room_master_id', $id)
                                    ->where('t_rooms_id', $roomId)
                                    ->orderBy('detail_rooms.id')
                                    ->get();
            $controllRoom = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('room_master_id', $id)
                                    ->where('t_rooms_id', $roomId)
                                    ->select('rooms.id', 'rooms.room_name', 'games.game_name', 'rooms.description_room', 'rooms.max_players', 'rooms.date',)
                                    ->first();

            $detailRoom = view('components/ComRoomControl', compact('detailRoomAsMasters'))->render();
            $detailRoomEdit = view('components/detailRoomControl', compact('controllRoom', 'roomMaster'))->render();

            return response()->json(['htmlControll' => $detailRoom, 'htmlRoomEdit' => $detailRoomEdit]);
        }
        $roomMaster = Detail_room::where('room_master_id', '=', $id)
                                ->where('t_players_id', '=', $id)
                                ->where('t_rooms_id', '=', $roomId)
                                ->first();
        $joinRoom = Detail_room::where('t_players_id', '=', $id)
                                ->where('t_rooms_id', '=', $roomId)
                                ->first();
        $controllRoomForMember = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('detail_rooms.t_rooms_id', $roomId)
                                    ->where('detail_rooms.t_players_id', $id)
                                    ->select('rooms.id', 'rooms.room_name', 'games.game_name', 'rooms.description_room', 
                                            'rooms.max_players', 'rooms.date', 'detail_rooms.t_players_id', 'detail_rooms.t_rooms_id')
                                    ->first();
                                    // dd($controllRoomForMember);
        $detailRoomAsMember = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                    ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                    ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                    ->where('detail_rooms.t_players_id', $id)
                                    ->where('detail_rooms.t_rooms_id', $roomId)
                                    ->first();
                                    
        if($detailRoomAsMember != null){
            $roomId = $detailRoomAsMember->t_rooms_id;
            $rm = $detailRoomAsMember->room_master_id;
            $dataMembers = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                        ->join('games', 'games.id', '=', 'detail_rooms.t_games_id')
                                        ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                        ->where('detail_rooms.t_rooms_id', $roomId)
                                        ->where('detail_rooms.room_master_id', $rm)
                                        ->select('players.id', 'players.player_name', 'players.exp', 'status')
                                        ->orderBy('detail_rooms.id')
                                        ->get();
            // dd($dataMembers);
        }else{
            $dataMembers=null;
        }
        
        $detailRoomMember = view('components/memberRoomControl', compact('dataMembers'))->render();
        $detailRoomEditMember = view('components/detailRoomControl', compact('controllRoomForMember', 'joinRoom', 'roomMaster'))->render();
        // dd($detailRoomEditMember);
        return response()->json(['htmlControllRoomMember' => $detailRoomMember, 'htmlRoomEditMember' => $detailRoomEditMember]);
    }

    public function pagegamemodal(Request $req) {
        return view('pageGame.pageGameModal');
    } 

    public function filterGames(Request $request)
    {
        // dd($request->all());
        $searchGame             = $request->search;
        $selectRowDifficulty    = $request->selectRowDifficulty;
        $selectRowMaxp          = $request->selectRowMaxp;
        $selectRowDur           = $request->selectRowDur;
        $selectRowAge           = $request->selectRowAge;
        $searchGame = $searchGame[0];
        // dd($searchGame);
        if($selectRowMaxp != null){
            $maxP   = $selectRowMaxp[0];
        }
        if($selectRowDur != null){
            $dur    = $selectRowDur[0];
        }
        if($selectRowAge != null){
            $age    = $selectRowAge[0];
        }

        if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
            && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!=""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowAge!=""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($searchGame!="" && $selectRowDifficulty!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowMaxp!="" 
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!="" && $selectRowAge!=""){
            // dd($searchGame);
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame == null && $selectRowMaxp!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            // dd($searchGame);
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowAge!=""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!=""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!=""){
            if($maxP <= 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame!="" && $selectRowDifficulty!=""
        && $selectRowDur!=""){
            if($dur == 30){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowAge){
            if($age == 0){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame !=  "" && $selectRowDifficulty != ""){
            $games = Game::where('game_name', '=', $searchGame)
                            ->where('difficulty_game', $selectRowDifficulty)
                            ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($searchGame !=  "" && $selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame!="" && $selectRowDur!=""){
            if($dur == 30){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame!="" && $selectRowAge != ""){
            if($age == 0){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowDur != ""){
            if($dur == 30){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowAge != ""){
            if($age == 0){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($selectRowMaxp != "" && $selectRowDur != ""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($selectRowMaxp != "" && $selectRowAge != ""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($selectRowDur != "" && $selectRowAge != ""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame != ""){
            $games = Game::where('game_name', '=', $searchGame)
                            ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($selectRowDifficulty != ""){
            $games = Game::where('difficulty_game', $selectRowDifficulty)
                        ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($selectRowDur != ""){
            if($dur == 30){
                $games = Game::where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($selectRowAge != ""){
            if($age == 0){
                $games = Game::where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }
    }
}