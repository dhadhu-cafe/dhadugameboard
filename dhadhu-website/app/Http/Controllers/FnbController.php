<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Fnb;
use App\Models\Categories;
use App\Models\Media;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;
use File;
Use \Carbon\Carbon;



class FnbController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $foods = Fnb::select('id','name_fnb','type_fnb','price_fnb', 'description_fnb', 'image_fnb')->paginate(10);
        $categoriesFoods = categories::where('category_type','menu FnB')->get();
        // dd($foods);
        return view('pages/page_fnb', compact('foods','categoriesFoods', 'admin'));
    }
    
    public function Store(Request $request)
    {
        // dd($request->all());
        $dishName = $request->dishName;
        $image = $request->imageFnb;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $typeDish = $request->typeDish;

        $newDish = new Fnb;
        $newDish->name_fnb = $request->dishName;
        $newDish->type_fnb = $request->typeDish;
        $newDish->price_fnb = $request->priceDish;
        $newDish->description_fnb = $request->descriptionDish;
        $newDish->image_fnb = $timeOriginalName;
        $newDish->save();
        $newImage = new Media;
        $newImage->original_name = $timeOriginalName;
        $newImage->image_name = $dishName;
        $newImage->image_category = $typeDish;
        $newImage->save();

        return redirect()->back()->with('success', 'berhasil Input Data!');
    }

    public function destroy(Request $request)
    {
        $id = $request->deleteMenuId;
        $selectedData = Fnb::where('id', $id)->first();
        $imageName = $selectedData->image_fnb;
        $oldDestinationPath = public_path().'/gambar/'.$imageName;
        $newDestinationPath = public_path().'/delete/'.$imageName;
        $move = File::move($oldDestinationPath, $newDestinationPath);
        if($move){
          $deleteFile = Fnb::where('id',$id)->delete();
          if($deleteFile){
              return [
                  'result' => 'success',
                  'message' => 'Berhasil menghapus data!',
              ];
          }else{
              return [
                  'result' => 'error',
                  'message' => 'Error saat menghapus data!',
              ];
          }
        }else{
          return [
            'result' => 'error',
            'message' => 'Kesalahan saat menghapus gambar!',
        ];
        }
        
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $id = Crypt::decryptString($id);
        $prevImage = $request->prevImage;
        // dd($prevImage);
        $oldDestinationPath = public_path().'/gambar/'.$prevImage;
        // dd($oldDestinationPath);
        $newDestinationPath = public_path().'/delete/'.$prevImage;
        if($oldDestinationPath == true){
            $move = File::move($oldDestinationPath, $newDestinationPath);
        }else{
            return [
                'result' => 'error',
                'message' => 'Kesalahan saat mengedit gambar!',
            ];
        }

        $image = $request->imageFnb;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $editFnb = Fnb::find($id);
        $editFnb->name_fnb          = $request->nameFnb;
        $editFnb->type_fnb          = $request->typeFnb;
        $editFnb->price_fnb         = $request->priceFnb;
        $editFnb->description_fnb   = $request->descFnb;
        $editFnb->image_fnb         = $timeOriginalName;
        $editFnb->save();

        return redirect()->back()->with('success', 'berhasil edit Data Fnb!');
    }
}
