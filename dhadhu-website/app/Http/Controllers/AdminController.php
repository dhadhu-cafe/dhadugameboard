<?php

namespace App\Http\Controllers;

use App\Models\Login;
use App\Models\Admin;
use App\Models\INfo;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Crypt;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin/loginadmin');
    }

    public function loginAdmin(Request $request)
    {
        $adminName = $request->username;
        $password = $request->password;

        $loginAdmin = Admin::where('admin_name', $adminName)->where('password', $password)->first();
        $id = $loginAdmin->id;
        
        if($loginAdmin = true){
            session(['berhasil_login' => true]);
            $enkripsi = Crypt::encryptString($id);
            $url = 'http://127.0.0.1:8000/admin/'.$enkripsi;
            return Redirect::to($url)->with('success', 'Selamat datang '.$adminName);
        }
        return redirect()->back()->with('error', 'Please check your data again!');
        // $id = $loginAdmin->id;
        // $enkripsi = Crypt::encryptString($id);
        // $url = 'http://127.0.0.1:8000/admin/'.$enkripsi;
        // return Redirect::to($url)->with('success', 'Selamat datang '.$adminName);
    }
public function logoutAdmin(Request $request){
    $request->session()->flush();
    return redirect('/loginadmin');//

}
    public function tampilan(Request $request = null, $id = '')
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $info = Info::select('id','kontak','Alamat','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu')->paginate(1);
        return view('adminpage', compact('admin','info'));
    }
    public function UpdateInfo(Request $request,$id)
    {  
        $updateInfo = info::find($id);
        $updateInfo->kontak = $request->Kontak;
        $updateInfo->Alamat= $request->Alamat;
        $updateInfo->Senin= $request->Senin;
        $updateInfo->Selasa= $request->Selasa;
        $updateInfo->Rabu= $request->Rabu;
        $updateInfo->Kamis= $request->Kamis;
        $updateInfo->Jumat= $request->Jumat;
        $updateInfo->Sabtu= $request->Sabtu;
        $updateInfo->Minggu= $request->Minggu;
        $updateInfo->save();
        return redirect()->back()->with('success', 'berhasil edit Data!'); 
    }

}
