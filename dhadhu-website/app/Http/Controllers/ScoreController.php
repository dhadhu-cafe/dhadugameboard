<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Score;
use App\Models\Game;
use App\Models\Player;
use App\Models\Detail_play;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class ScoreController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $scores = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                        ->join('players', 'players.id', '=', 'scores.t_players_id')
                        ->select('scores.id', 'players.player_name', 'games.game_name', 'scores.score', 'scores.playtime')
                        ->paginate(10);
        // dd($scores);
        return view('pages/page_score', compact('scores', 'admin'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $playerName = $request->player_name;
        $gameName = $request->game_name;
        $dataGame = Game::where('game_name', $gameName)->first();
        if($dataGame == null){
            return [
                'result' => 'error',
                'message' => 'Data game tidak di temukan!',
            ];
        }
        $dataPLayer = Player::where('player_name', $playerName)->first();
        if($dataPLayer == null){
            return [
                'result' => 'error',
                'message' => 'Data player tidak di temukan!',
            ];
        }
        
        $gameId = $dataGame->id;
        $playerId = $dataPLayer->id;
        $cekDetailScore = Score::where('t_games_id', $gameId)
                                ->where('t_players_id', $playerId)
                                ->orderBy('created_at', 'desc')
                                ->first();
        
        // dd($cekDetailScore);
        if($cekDetailScore == true){
            $lastestScore = $cekDetailScore->total_score;
            $lastestTime = $cekDetailScore->playtime;
            $newScore = new Score;
            $newScore->t_games_id   = $gameId;
            $newScore->t_players_id = $playerId;
            $newScore->score        = $request->score;
            $newScore->total_score  = $lastestScore + $request->score;
            $newScore->playtime     = $lastestTime + $request->playtime;
            $newScore->save();
            if($newScore == true){
                $countPlayed = Score::where('t_games_id', $gameId)
                                    ->where('t_players_id', $playerId)
                                    ->count();
                // dd($countPlayed);
                $countPlayedGame = Detail_play::where('t_games_id', $gameId)
                                            ->where('t_players_id', $playerId)
                                            ->first();

                if($countPlayedGame == null){
                    $countPlayedGame = new Detail_play;
                    $countPlayedGame->t_games_id    = $gameId;
                    $countPlayedGame->t_players_id  = $playerId;
                    $countPlayedGame->played        = $countPlayed;
                    $countPlayedGame->save();
                }
                $countPlayedGame->t_games_id    = $gameId;
                $countPlayedGame->t_players_id  = $playerId;
                $countPlayedGame->played        = $countPlayed;
                $countPlayedGame->save();
                if($countPlayedGame == true){
                    $countPlayedGameById = Score::where('t_games_id', $gameId)->count();
                    $updateTotalPlayGame = Game::where('id', $gameId)->first();
                    $updateTotalPlayGame->total_play = $countPlayedGameById;
                    $updateTotalPlayGame->save();
                    if($updateTotalPlayGame == true){
                        return [
                            'result' => 'success',
                            'message' => 'Berhasil memasukkan data!',
                        ];
                    }else{
                        return [
                            'result' => 'error',
                            'message' => 'Error saat memasukkan data!',
                        ];
                    }

                }else{
                    return [
                        'result' => 'error',
                        'message' => 'Error saat memasukkan data!',
                    ];
                }

            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat memasukkan data!',
                ];
            }  
        }

        $newScore = new Score;
        $newScore->t_games_id   = $gameId;
        $newScore->t_players_id = $playerId;
        $newScore->score        = $request->score;
        $newScore->total_score  = $request->score;
        $newScore->playtime     = $request->playtime;
        $newScore->save();
        if($newScore == true){
            $countPlayed = Score::where('t_games_id', $gameId)
                                ->where('t_players_id', $playerId)
                                ->count();
            // dd($countPlayed);
            $countPlayedGame = Detail_play::where('t_games_id', $gameId)
                                        ->where('t_players_id', $playerId)
                                        ->first();

            if($countPlayedGame == null){
                $countPlayedGame = new Detail_play;
                $countPlayedGame->t_games_id    = $gameId;
                $countPlayedGame->t_players_id  = $playerId;
                $countPlayedGame->played        = $countPlayed;
                $countPlayedGame->save();
            }
            $countPlayedGame->t_games_id    = $gameId;
            $countPlayedGame->t_players_id  = $playerId;
            $countPlayedGame->played        = $countPlayed;
            $countPlayedGame->save();
            if($countPlayedGame == true){
                $countPlayedGameById = Score::where('t_games_id', $gameId)->count();
                $updateTotalPlayGame = Game::where('id', $gameId)->first();
                $updateTotalPlayGame->total_play = $countPlayedGameById;
                $updateTotalPlayGame->save();
                if($updateTotalPlayGame == true){
                    return [
                        'result' => 'success',
                        'message' => 'Berhasil memasukkan data!',
                    ];
                }else{
                    return [
                        'result' => 'error',
                        'message' => 'Error saat memasukkan data!',
                    ];
                }

            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat memasukkan data!',
                ];
            }

        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat memasukkan data!',
            ];
        }  

        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $id = $request->deleteScoreId;
        // dd($id);
        $deleteFile = Score::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }
    public function update(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $update_c = score::find($id);
        $update_c->t_games_id = $request->CGameIdUpdate;
        $update_c->t_players_id= $request->CPlayerIdUpdate;
        $update_c->total_score= $request->CScoreUpdate;
        $update_c->playtime= $request->CPlaytimeUpdate;
        $update_c->save();
        return redirect()->back()->with('success', 'berhasil edit Data Score!');
    }
}
