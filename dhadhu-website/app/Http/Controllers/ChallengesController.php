<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Challenge;
use App\Models\Challenges_record;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class ChallengesController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $challenge = Challenge::select('id', 'challenge_name', 'description_challenge', 'exp')->paginate(10);
        
        return view('pages/page_challenge', compact('challenge', 'admin'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $newChallenge = new Challenge;
        $newChallenge->Challenge_name         = $request->chalname;
        $newChallenge->description_Challenge  = $request->chaldesc;
        $newChallenge->exp                    = $request->chalexp;
        $newChallenge->save();
        // $detailChallenge = new Challenges_record;
        // $detailChallengeData = Challenge::get('id')->last();
        // $FirstCustomDetailChallengeId = Str::beforeLast($detailChallengeData, '}');
        // $detailChallengeRecordId = Str::afterLast($FirstCustomDetailChallengeId, ':');
        // $detailChallenge->challenges_id = $detailChallengeRecordId;
        // $detailChallenge->t_players_id = '0';
        // $detailChallenge->exp = '0';
        // $detailChallenge->status = 'belum selesai';
        // $detailChallenge->save();

        if($newChallenge){
            return [
                'result' => 'success',
                'message' => 'Berhasil memasukkan data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat memasukkan data!',
            ];
        }
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $id = $request->deleteChallengeId;
        // dd($id);
        $deleteFile = Challenge::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }
    public function update(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $update_c = challenge::find($id);
        $update_c->challenge_name           = $request->CNameUpdate;
        $update_c->description_challenge    = $request->CDescUpdate;
        $update_c->exp                      = $request->CExpUpdate;
        $update_c->save();
        return redirect()->back()->with('success', 'berhasil edit Data quest!');
    }
}
