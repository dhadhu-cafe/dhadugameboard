<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fnb;
use App\Models\Game;
use App\Models\Event;
use App\Models\Challenge;
use App\Models\Player;
use App\Models\Score;
use App\Models\Room;
use App\Models\INfo;
use App\Models\Categories;


class GuestController extends Controller
{
    public function guest(Type $var = null)
    {
        $categoryMenus = Categories::where('category_type', 'Menu Fnb')->get();
        $menus = Fnb::select('id', 'name_fnb', 'type_fnb',
                            'price_fnb', 'description_fnb', 'image_fnb')
                            ->get();

        //BEGIN::ROLLER
        $games = Game::select('id','game_name', 'category_game',	'age',
                                'jml_player', 'durasi', 'total_play', 
                                'difficulty_game', 'image_game', 
                                'description_game')->get();
        $recentPlay = null;
        // dd($recentPlay);
        $topGames = Game::orderBy('total_play', 'desc')->paginate(3);
        $favGame  = null;
        
        //play
        $newRooms = Room::select('rooms.id', 'rooms.room_name', 'rooms.description_room', 'rooms.t_games_id', 'games.game_name', 'rooms.max_players')
                        // ->join('detail_rooms', 'detail_rooms.id', '=', 'rooms.t_detail_rooms_id')
                        ->join('games', 'games.id', '=', 'rooms.t_games_id')
                        ->join('players', 'players.id', '=', 'rooms.t_players_id')
                        ->get();
        //play
        //END::ROLLER

        // BEGIN::PROMOS AND MORE
        $eventsDashboard = Event::select('id', 'event_name', 'category',
                                          'image_event', 'description_event')
                                 ->orderBy('id', 'desc')
                                 ->paginate(3);
        $events = Event::select('id', 'event_name', 'category', 'image_event', 'description_event')->get();
        // END::PROMOS AND MORE

        // BEGIN::PROMOS AND MORE
        $eventAndMores = Event::where('category', 'event')->get();
        $promoAndMores = Event::where('category', 'promo')->get();
        $newAndMores = Event::where('category', 'new')->get();
        // END::PROMOS AND MORE
        
        //BEGIN::CHALLANGE
        $challenges = Challenge::all();
        //END::CHALLANGE        

        //BEGIN::SCORE
        $filterLeaderboard='1';
        $leaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('total_score', 'desc')
                                ->paginate(10);
        $highScoreLeaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('score', 'desc')
                                ->paginate(10);
        $leaderboardsTime = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('playtime', 'desc')
                                ->paginate(10);

        //BEGIN:: info
        $info = info:: select('kontak','Alamat','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu')->get();
        //END:: info

        //END::SCORE
        return view('welcome', compact( 'menus', 'categoryMenus', 'games',
                                        'topGames', 'events', 'eventAndMores', 'promoAndMores',
                                        'newAndMores', 'challenges', 'leaderboards',
                                        'leaderboardsTime', 'newRooms', 'eventsDashboard',
                                        'info','recentPlay','highScoreLeaderboards', 'favGame'));
    }

    public function filter(Request $request)
    {
        $filterLeaderboard = $request->filterLeaderboard;
        // dd($filterLeaderboard);

        if($filterLeaderboard == null){
            $filterLeaderboard = "1";
        }else if($filterLeaderboard == "Romance"){
            $filterLeaderboard = "1";
        }else if($filterLeaderboard == "RPG"){
            $filterLeaderboard = "2";
        }else if($filterLeaderboard == "Action"){
            $filterLeaderboard = "3";
        }else if($filterLeaderboard == "Strategi"){
            $filterLeaderboard = "4";
        }
        $leaderboards = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('total_score', 'desc')
                                ->paginate(10);
        // dd($leaderboards);
        $leaderboardsTime = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                                ->join('players', 'players.id', '=', 'scores.t_players_id')
                                ->where('t_games_id', $filterLeaderboard)
                                ->orderBy('playtime', 'desc')
                                ->paginate(10);
        // dd($leaderboardsTime);
        
        $filterLd = view('components/leaderboardScore', compact('leaderboards'))->render();
        $filterLdTime = view('components/leaderboardTime', compact('leaderboardsTime'))->render();
        // dd($filterLd);
        return response()->json(['htmlLd' => $filterLd, 'htmlLdTime' => $filterLdTime]);
    }

    public function filterEvents(Request $request)
    {
        // dd($request->all());
        $filterMonths = $request->months;
        $filterEvents = $request->events;
        if($filterMonths!="" && $filterEvents!=""){
            $events = Event::where('category', $filterEvents)
                            ->whereMonth('created_at', $filterMonths)
                            ->get();

            if($events){
               $filtered = view('components/filterEvents', compact('events'))->render();

               return response()->json(['htmlFilterEvents' => $filtered]);
            }else{
                return redirect()->back()->with('warning', 'No data found!');
            }
        }else if($filterMonths!=""){
            $events = Event::whereMonth('created_at', $filterMonths)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();
    
                return response()->json(['htmlFilterEvents' => $filtered]);
                }else{
                    return redirect()->back()->with('warning', 'No data found!');
                }
        }else if($filterEvents!=""){
            $events = Event::where('category', $filterEvents)
                            ->get();
            if($events){
                $filtered = view('components/filterEvents', compact('events'))->render();
    
                    return response()->json(['htmlFilterEvents' => $filtered]);
                }else{
                    return redirect()->back()->with('warning', 'No data found!');
                }
        }      
    }

    public function filterGames(Request $request)
    {
        // dd($request->all());
        $searchGame             = $request->search;
        $selectRowDifficulty    = $request->selectRowDifficulty;
        $selectRowMaxp          = $request->selectRowMaxp;
        $selectRowDur           = $request->selectRowDur;
        $selectRowAge           = $request->selectRowAge;
        $searchGame = $searchGame[0];
        // dd($searchGame);
        if($selectRowMaxp != null){
            $maxP   = $selectRowMaxp[0];
        }
        if($selectRowDur != null){
            $dur    = $selectRowDur[0];
        }
        if($selectRowAge != null){
            $age    = $selectRowAge[0];
        }

        if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
            && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!=""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowAge!=""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($searchGame!="" && $selectRowDifficulty!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('game_name', '=', $searchGame)
                                    ->where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowMaxp!="" 
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('game_name', '=', $searchGame)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!="" && $selectRowAge!=""){
            // dd($searchGame);
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('difficulty_game', $selectRowDifficulty)
                                        ->where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame == null && $selectRowMaxp!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            // dd($searchGame);
            if($dur == 30){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '<=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    if($maxP <= 4){
                        // dd($age);
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 17){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '<=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }else if($age == 18){
                    // dd($age);
                    if($maxP <= 4){
                        $games = Game::where('jml_player', '=', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }else if($maxP > 4){
                        $games = Game::where('jml_player', '>', $maxP)
                                        ->where('durasi', '>=', $dur)
                                        ->where('age', '>=', $age)
                        ->get();
                        if($games){
                        $filtered = view('components/filterGame', compact('games'))->render();
                        
                        return response()->json(['htmlFilterGames' => $filtered]);
                        }
                    }
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty!=""
        && $selectRowDur!="" && $selectRowAge!=""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowAge!=""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($searchGame == null && $selectRowDifficulty!="" && $selectRowMaxp!="" 
        && $selectRowDur!=""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('difficulty_game', $selectRowDifficulty)
                                    ->where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowMaxp!=""){
            if($maxP <= 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame!="" && $selectRowDifficulty!=""
        && $selectRowDur!=""){
            if($dur == 30){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame!="" && $selectRowDifficulty!="" && $selectRowAge){
            if($age == 0){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame !=  "" && $selectRowDifficulty != ""){
            $games = Game::where('game_name', '=', $searchGame)
                            ->where('difficulty_game', $selectRowDifficulty)
                            ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($searchGame !=  "" && $selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame!="" && $selectRowDur!=""){
            if($dur == 30){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame!="" && $selectRowAge != ""){
            if($age == 0){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('game_name', '=', $searchGame)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowDur != ""){
            if($dur == 30){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($searchGame == null && $selectRowDifficulty != "" && $selectRowAge != ""){
            if($age == 0){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('difficulty_game', $selectRowDifficulty)
                                ->where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($selectRowMaxp != "" && $selectRowDur != ""){
            if($dur == 30){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('durasi', '<=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('durasi', '>=', $dur)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($selectRowMaxp != "" && $selectRowAge != ""){
            if($age == 0){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 17){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($age == 18){
                if($maxP <= 4){
                    $games = Game::where('jml_player', '=', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($maxP > 4){
                    $games = Game::where('jml_player', '>', $maxP)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }
        }else if($selectRowDur != "" && $selectRowAge != ""){
            if($dur == 30){
                if($age == 0){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('durasi', '<=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            }else if($dur == 31){
                if($age == 0){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '>', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 17){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '<=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }else if($age == 18){
                    $games = Game::where('durasi', '>=', $dur)
                                    ->where('age', '>=', $age)
                    ->get();
                    if($games){
                    $filtered = view('components/filterGame', compact('games'))->render();
                    
                    return response()->json(['htmlFilterGames' => $filtered]);
                    }
                }
            } 
        }else if($searchGame != ""){
            $games = Game::where('game_name', '=', $searchGame)
                            ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($selectRowDifficulty != ""){
            $games = Game::where('difficulty_game', $selectRowDifficulty)
                        ->get();
            if($games){
            $filtered = view('components/filterGame', compact('games'))->render();

            return response()->json(['htmlFilterGames' => $filtered]);
            }
        }else if($selectRowMaxp != ""){
            if($maxP <= 4){
                $games = Game::where('jml_player', '=', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($maxP > 4){
                $games = Game::where('jml_player', '>', $maxP)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }else if($selectRowDur != ""){
            if($dur == 30){
                $games = Game::where('durasi', '<=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($dur == 31){
                $games = Game::where('durasi', '>=', $dur)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            } 
        }else if($selectRowAge != ""){
            if($age == 0){
                $games = Game::where('age', '>', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 17){
                $games = Game::where('age', '<=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }else if($age == 18){
                $games = Game::where('age', '>=', $age)
                ->get();
                if($games){
                $filtered = view('components/filterGame', compact('games'))->render();
                
                return response()->json(['htmlFilterGames' => $filtered]);
                }
            }
        }
    }
}

