<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Admin;
use App\Models\Score;
Use \Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class PlayerController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $player = player::select('id','player_name','fname','lname','email','password','cfmPassword','gender', 'player_image', 'exp')->paginate(10);
        // dd($player);
        
        return view('pages/page_player', compact('player', 'admin'));
    }
    public function destroy(Request $request)
    {
        $id = $request->deleteCategoriesId;
        // dd($id);
        $deleteFile = player::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $id = Crypt::decryptString($id);
        $gender = $request->GenderUpdate;
        if($gender == 1){
            $genderString = "male";
        }else if($gender == 2){
            $genderString = "female";
        }
        $update_player = player::find($id);
        $update_player->player_name = $request->NicknameUpdate;
        $update_player->fname = $request->LNameUpdate;
        $update_player->lname= $request->FNameUpdate;
        $update_player->email = $request->EmailUpdate;
        $update_player->password = $request->PasswordUpdate;
        $update_player->cfmPassword = $request->PasswordUpdate;
        $update_player->gender = $genderString;
        $update_player->exp = $request->LevelUpdate;
        $update_player->save();
        return redirect()->back()->with('success', 'berhasil edit Data Player!');
    }

    public function profileManager(Request $request, $id) 
    {
        // dd($id);
        $id = Crypt::decryptString($id);
        $dataPlayer = Player::where('id', '=', $id)->first();
        // dd($dataPlayer);
        $historys = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                        ->join('players', 'players.id', '=', 'scores.t_players_id')
                        ->where('players.id', '=', $id)
                        ->select('scores.id', 'players.player_name', 'games.game_name', 'scores.score', 'scores.playtime', 'scores.total_score', 'scores.created_at')
                        ->paginate(10);

        // dd($history);
        $user = Player::where('id', '=', $id)->first();
        return view('ProfileManager.profile', compact('dataPlayer', 'historys', 'user'));
    }

    public function editProfileManager(Request $request, $id)
    {
        // dd($request->all());
        $id = Crypt::decryptString($id);
        $validator = Validator::make($request->all(), [
            'player_name' => 'required|min:4',
            'password' => 'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/u',
        ]);
        if ($validator->fails()) {
            $messages = $validator->errors();
            $error_messages = '';
            foreach ($messages->all() as $message) {
                $error_messages = $error_messages . $message;
            }
            return redirect()->back()->with('warning', $error_messages);
        }

        $playerName = $request->player_name;
        // dd($playerName);
        $cekNick = Player::where('player_name', $playerName)->select('id')->orderBy('id', 'desc')->first();
        if(!isset($cekNick)){

            $email = $request->email;
            // // dd($email);
            // $cekEmail = Player::where('email', $email)->select('id')->orderBy('id', 'desc')->first();
            
            // if($cekEmail->id != $id ){
            //     return redirect()->back()->with('warning', 'Email sudah di gunakan! *Silahkan gunakan Email yang lain.');
            // }

            $password = $request->password;
            $cfmPassword = $request->cfmPassword;
            if($password != $cfmPassword){
                return redirect()->back()->with('warning', 'Password and Confirm password is diferent!');
            }

            $profileImage = $request->profileImage;
            $prevImage = $request->prevImage;
            // dd($profileImage);
            if($profileImage != null){
                if($prevImage == null){
                    $originalName = $profileImage->getClientOriginalName();
                    $timestamp = Carbon::now()->format('YmdHisu'); 
                    $timeOriginalName = $timestamp.'_'.$originalName;
                    $moved = $profileImage->move(public_path()."/gambar", $timeOriginalName);
                    // dd($moved);
                    if($moved == true){
                        $dataPlayer = Player::where('id', '=', $id)->first();
                        // dd($dataPlayer);
                        $dataPlayer->player_name = $request->player_name;
                        $dataPlayer->fname = $request->fname;
                        $dataPlayer->lname = $request->lname;
                        $dataPlayer->email = $request->email;
                        $dataPlayer->password = $request->password;
                        $dataPlayer->cfmPassword = $request->cfmPassword;
                        $dataPlayer->player_image = $timeOriginalName;
                        $dataPlayer->save();
                        return redirect()->back()->with('success', 'berhasil merubah data diri!');
                    }  
                }
                $oldDestinationPath = public_path().'/gambar/'.$prevImage;
                $newDestinationPath = public_path().'/delete/'.$prevImage;
                // dd($oldDestinationPath);
                if($oldDestinationPath == true){
                    $move = File::move($oldDestinationPath, $newDestinationPath);
                }else{
                    return [
                        'result' => 'error',
                        'message' => 'Kesalahan saat mengedit gambar!',
                    ];
                }
                $originalName = $profileImage->getClientOriginalName();
                $timestamp = Carbon::now()->format('YmdHisu'); 
                $timeOriginalName = $timestamp.'_'.$originalName;
                $moved = $profileImage->move(public_path()."/gambar", $timeOriginalName);
                // dd($moved);
                if($moved == true){
                    $dataPlayer = Player::where('id', '=', $id)->first();
                    // dd($dataPlayer);
                    $dataPlayer->player_name = $request->player_name;
                    $dataPlayer->fname = $request->fname;
                    $dataPlayer->lname = $request->lname;
                    $dataPlayer->email = $request->email;
                    $dataPlayer->password = $request->password;
                    $dataPlayer->cfmPassword = $request->cfmPassword;
                    $dataPlayer->player_image = $timeOriginalName;
                    $dataPlayer->save();
                    return redirect()->back()->with('success', 'berhasil merubah data diri!');
                }  
            }
            else if($prevImage != null){
                $dataPlayer = Player::where('id', '=', $id)->first();
                // dd($dataPlayer);
                $dataPlayer->player_name = $request->player_name;
                $dataPlayer->fname = $request->fname;
                $dataPlayer->lname = $request->lname;
                $dataPlayer->email = $request->email;
                $dataPlayer->password = $request->password;
                $dataPlayer->cfmPassword = $request->cfmPassword;
                $dataPlayer->player_image = $prevImage;
                $dataPlayer->save();
                return redirect()->back()->with('success', 'berhasil merubah data diri!');
            }
        }else{
            if($cekNick->id != $id ){
                return redirect()->back()->with('warning', 'Nick name sudah di gunakan! *Silahkan gunakan Nick name yang lain.');
            }
    
            $email = $request->email;
            // $cekEmail = Player::where('email', $email)->select('id')->orderBy('id', 'desc')->first();
            
            // if($cekEmail->id != $id ){
            //     return redirect()->back()->with('warning', 'Email sudah di gunakan! *Silahkan gunakan Email yang lain.');
            // }
    
            $password = $request->password;
            $cfmPassword = $request->cfmPassword;
            if($password != $cfmPassword){
                return redirect()->back()->with('warning', 'Password and Confirm password is diferent!');
            }
            
            $profileImage = $request->profileImage;
            $prevImage = $request->prevImage;
            if($profileImage != null){
                if($prevImage == null){
                    $originalName = $profileImage->getClientOriginalName();
                    $timestamp = Carbon::now()->format('YmdHisu'); 
                    $timeOriginalName = $timestamp.'_'.$originalName;
                    $moved = $profileImage->move(public_path()."/gambar", $timeOriginalName);
                    // dd($moved);
                    if($moved == true){
                        $dataPlayer = Player::where('id', '=', $id)->first();
                        // dd($dataPlayer);
                        $dataPlayer->player_name = $request->player_name;
                        $dataPlayer->fname = $request->fname;
                        $dataPlayer->lname = $request->lname;
                        $dataPlayer->email = $request->email;
                        $dataPlayer->password = $request->password;
                        $dataPlayer->cfmPassword = $request->cfmPassword;
                        $dataPlayer->player_image = $timeOriginalName;
                        $dataPlayer->save();
                        return redirect()->back()->with('success', 'berhasil merubah data diri!');
                    }  
                }
                $oldDestinationPath = public_path().'/gambar/'.$prevImage;
                $newDestinationPath = public_path().'/delete/'.$prevImage;
                // dd($oldDestinationPath);
                if($oldDestinationPath == true){
                    $move = File::move($oldDestinationPath, $newDestinationPath);
                }else{
                    return [
                        'result' => 'error',
                        'message' => 'Kesalahan saat mengedit gambar!',
                    ];
                }
                $originalName = $profileImage->getClientOriginalName();
                $timestamp = Carbon::now()->format('YmdHisu'); 
                $timeOriginalName = $timestamp.'_'.$originalName;
                $moved = $profileImage->move(public_path()."/gambar", $timeOriginalName);
                // dd($moved);
                if($moved == true){
                    $dataPlayer = Player::where('id', '=', $id)->first();
                    // dd($dataPlayer);
                    $dataPlayer->player_name = $request->player_name;
                    $dataPlayer->fname = $request->fname;
                    $dataPlayer->lname = $request->lname;
                    $dataPlayer->email = $request->email;
                    $dataPlayer->password = $request->password;
                    $dataPlayer->cfmPassword = $request->cfmPassword;
                    $dataPlayer->player_image = $timeOriginalName;
                    $dataPlayer->save();
                    return redirect()->back()->with('success', 'berhasil merubah data diri!');
                }  
            }
            else if($prevImage != null){
                $dataPlayer = Player::where('id', '=', $id)->first();
                // dd($dataPlayer);
                $dataPlayer->player_name = $request->player_name;
                $dataPlayer->fname = $request->fname;
                $dataPlayer->lname = $request->lname;
                $dataPlayer->email = $request->email;
                $dataPlayer->password = $request->password;
                $dataPlayer->cfmPassword = $request->cfmPassword;
                $dataPlayer->player_image = $prevImage;
                $dataPlayer->save();
                return redirect()->back()->with('success', 'berhasil merubah data diri!');
            }
        }

        $dataPlayer = Player::where('id', '=', $id)->first();
        // dd($dataPlayer);
        $dataPlayer->player_name = $request->player_name;
        $dataPlayer->fname = $request->fname;
        $dataPlayer->lname = $request->lname;
        $dataPlayer->email = $request->email;
        $dataPlayer->password = $request->password;
        $dataPlayer->cfmPassword = $request->cfmPassword;
        $dataPlayer->player_image = $profileImage;
        $dataPlayer->save();
        return redirect()->back()->with('success', 'berhasil merubah data diri!');
    }

}
