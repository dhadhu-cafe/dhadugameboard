<?php

namespace App\Http\Controllers;

use App\Models\RegisterPlayer;
use App\Models\Player;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $allData = $request->all();
        // dd($allData);
        $validator = Validator::make($request->all(), [
            'nickName' => 'required|min:4',
            'password' => 'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/u',
        ]);
        if ($validator->fails()) {
            $messages = $validator->errors();
            $error_messages = '';
            foreach ($messages->all() as $message) {
                $error_messages = $error_messages . $message;
            }
            return redirect()->back()->with('error', $error_messages);
        }
        $playerName = $request->nickName;
        $nickNameUseds = Player::where('player_name', $playerName)->count();
        // dd($nickNameUseds);
        if($nickNameUseds != 0){
            return redirect()->back()->with('error', 'Nick name already used!');
        }else{
            $nickNameUseds = $playerName;
        }
        $email = $request->email;
        $allEmails = Player::where('email', $email)->count();
        if($allEmails != 0){
            return redirect()->back()->with('error', 'Email already used!');
        }else{
            $email;
        }
        
        $password = $request->password;
        $cfmPassword = $request->cfmPassword;
        if($password != $cfmPassword){
            return redirect()->back()->with('error', 'Please check your password!');
        }
        $gender = $request->gender;
        if($gender == null){
            return redirect()->back()->with('error', 'Please select gender!');
        }
        $level_player = '0';
        $newPlayer = new Player;
        $newPlayer->fname           = $request->fname;
        $newPlayer->lname           = $request->lname;
        $newPlayer->player_name     = $nickNameUseds;
        $newPlayer->email           = $email;
        $newPlayer->password        = $password;
        $newPlayer->cfmPassword     = $cfmPassword;
        $newPlayer->gender          = $gender;
        $newPlayer->exp             = $level_player;  
        $newPlayer->save();
        $url = 'http://127.0.0.1:8000/login';
        return Redirect::to($url)->with('success', 'Register Complete!');
        // return view('/login')->with('success', 'Success register!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegisterPlayer  $registerPlayer
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterPlayer $registerPlayer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegisterPlayer  $registerPlayer
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterPlayer $registerPlayer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegisterPlayer  $registerPlayer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisterPlayer $registerPlayer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegisterPlayer  $registerPlayer
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterPlayer $registerPlayer)
    {
        //
    }
}
