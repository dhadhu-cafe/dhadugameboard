<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Categories;
use App\Models\Media;
use File;
Use \Carbon\Carbon;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class EventsController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $events = event::select('id', 'event_name', 'category', 'description_event','image_event')->paginate(10);
        // dd($events);
        $categoriesPost = categories::where('category_type','postingan')->get();
        return view('pages/page_postingan', compact('events','categoriesPost', 'admin'));
    }
    public function store(Request $request)
    {
        $eventName = $request->eventName;
        $image = $request->imagePost;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $image->move(public_path()."/gambar", $timeOriginalName);
        $categoryPost = $request->categoryPost;
        $newEvent = new event;
        $newEvent->event_name = $request->eventName;
        $newEvent->category = $categoryPost;
        $newEvent->image_event = $timeOriginalName;
        $newEvent->description_event = $request->descriptionPost;
        $newEvent->save();
        $newImage = new Media;
        $newImage->original_name = $timeOriginalName;
        $newImage->image_name = $eventName;
        $newImage->image_category = $categoryPost;
        $newImage->save();
        // if($newEvent){
        //     return [
        //         'result' => 'success',
        //         'message' => 'Berhasil memasukkan data!',
        //     ];
        // }else{
        //     return [
        //         'result' => 'error',
        //         'message' => 'Error saat memasukkan data!',
        //     ];
        // }
        return redirect()->back()->with('success', 'berhasil Input Data!');
    }

    public function destroy(Request $request)
    {
        $id = $request->deletePostinganId;
        // dd($id);
        $selectedData = Event::where('id', $id)->first();
        $imageName = $selectedData->image_event;
        $oldDestinationPath = public_path().'/gambar/'.$imageName;
        $newDestinationPath = public_path().'/delete/'.$imageName;
        $move = File::move($oldDestinationPath, $newDestinationPath);
        if($move){
            $deleteFile = Event::where('id',$id)->delete();
            if($deleteFile){
                return [
                    'result' => 'success',
                    'message' => 'Berhasil menghapus data!',
                ];
            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat menghapus data!',
                ];
            }
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus gambar!',
            ];
        }
        
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $prevImage = $request->prevImage;
        $oldDestinationPath = public_path().'/gambar/'.$prevImage;
        $newDestinationPath = public_path().'/delete/'.$prevImage;
        if($oldDestinationPath == true){
            $move = File::move($oldDestinationPath, $newDestinationPath);
        }else{
            return [
                'result' => 'error',
                'message' => 'Kesalahan saat mengedit gambar!',
            ];
        }

        $image = $request->imagePost;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $editFnb = Event::find($id);
        $editFnb->event_name          = $request->titlePost;
        $editFnb->category            = $request->categoryPost;
        $editFnb->image_event         = $timeOriginalName;
        $editFnb->description_event   = $request->descriptionPost;
        $editFnb->save();

        return redirect()->back()->with('success', 'berhasil edit Data Event!');
    }
}
