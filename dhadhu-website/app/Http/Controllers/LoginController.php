<?php

namespace App\Http\Controllers;

use App\Models\Login;
use App\Models\Player;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Sentinel;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginUser(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $loginUser = Player::where('email', $email)->where('password', $password)->first();
        $playerName = $loginUser->player_name;
        $id = $loginUser->id;
        if($loginUser = true){
            session(['login_success'=>true]);
            $enkripsi = Crypt::encryptString($id);
            $url = 'http://127.0.0.1:8000/dashboard/'.$enkripsi;
            return Redirect::to($url)->with('success', 'Selamat datang '.$playerName);
        }
        return redirect()->back()->with('error', 'Please check your data again!');
    }

    
    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function show(Login $login)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function edit(Login $login)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Login $login)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Login  $login
     * @return \Illuminate\Http\Response
     */
    public function destroy(Login $login)
    {
        //
    }
}
