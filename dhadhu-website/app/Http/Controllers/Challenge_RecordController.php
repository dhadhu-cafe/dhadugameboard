<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Challenges_record;
use App\Models\Challenge;
use App\Models\Player;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class Challenge_RecordController extends Controller
{
    public function index(Request $request, $id)
    {   
        // dd($request->all());
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $challengeRecord = challenges_record::join('challenges', 'challenges.id', 'challenges_records.challenges_id')
                                            ->join('players', 'players.id', 'challenges_records.t_players_id')
                                            ->select('challenges_records.id', 'challenges_records.challenges_id', 'players.player_name',
                                                     'challenges_records.status', 'challenges.exp')->paginate(10);
        $challengeRecordDinamis = Challenge::select('id')->get();
        // dd($challengeRecord);
        
        return view('pages/page_challenge_record', compact('challengeRecord', 'challengeRecordDinamis', 'admin'));
    }
    public function store(Request $request)
    {
        // dd($request->all());
        
        $playerName = $request->CPlayerName;
        $dataPlayer = Player::where('player_name', $playerName)->first();
        if($dataPlayer == null){
            return [
                'result' => 'error',
                'message' => 'Data player tidak ditemukan!',
            ];
        }
        $playerId = $dataPlayer->id;
        $challengeId = $request->Cid;
        $cekStatus  = Challenges_record::where('challenges_id', $challengeId)->where('t_players_id', $playerId)->first();
        if($cekStatus){
            return [
                'result' => 'error',
                'message' => 'Player sudah menyelesaikan quest!',
            ];
        }
        $newChallengeRec = new Challenges_record;
        $newChallengeRec->challenges_id = $request->Cid;
        $newChallengeRec->t_players_id  = $playerId;
        $newChallengeRec->status        = $request->status;
        $newChallengeRec->save();
        if($newChallengeRec){
            $player = Player::Where('id', $playerId)->first();
            $playerIdExp = $player->id;
            $getExp = challenges_record::join('challenges', 'challenges.id', 'challenges_records.challenges_id')
                                        ->join('players', 'players.id', 'challenges_records.t_players_id')
                                        ->where('challenges_records.challenges_id', $challengeId)
                                        ->where('challenges_records.t_players_id', $playerIdExp)
                                        ->select('challenges.exp')->first();
                                        // dd($getExp);
            $exp = $getExp->exp;
            $lastesExp =    $player->exp;
            $newExp = $lastesExp + $exp;
            $player->exp = $newExp;
            $player->save();
            return [
                'result' => 'success',
                'message' => 'Berhasil memasukkan data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat memasukkan data!',
            ];
        }

        return redirect()->back();
    }
    public function destroy(Request $request)
    {
        $id = $request->deleteChallengeRecId;
        $dataChallengeRecord = Challenges_record::where('id', $id)->first();
        $playerId = $dataChallengeRecord->t_players_id;
        $challengeId = $dataChallengeRecord->challenges_id;
        $player = Player::Where('id', $playerId)->first();
        $playerIdExp = $player->id;
        $getExp = challenges_record::join('challenges', 'challenges.id', 'challenges_records.challenges_id')
                                    ->join('players', 'players.id', 'challenges_records.t_players_id')
                                    ->where('challenges_records.challenges_id', $challengeId)
                                    ->where('challenges_records.t_players_id', $playerIdExp)
                                    ->select('challenges.exp')->first();
                                    // dd($getExp);
        $exp = $getExp->exp;
        // dd($exp);
        $lastesExp =    $player->exp;
        $newExp = $lastesExp - $exp;
        $player->exp = $newExp;
        $player->save();
        $deleteFile = Challenges_record::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }

    // public function update(Request $request, $id)
    // {
    //     return redirect()->back()->with('Warning', 'Maaf sedang dalam perbaikan!');
    //     // dd($request->all());
    //     $id = Crypt::decryptString($id);
    //     $statusChallenge = $request->statusUpdate;
    //     // dd($id);
    //     if($statusChallenge == 1){
    //         $statusChallengeString = "Selesai";
    //     }else if($statusChallenge == 2){
    //         $statusChallengeString = "Belum Selesai";
    //     }
    //     $dataChallengeRecord = Challenges_record::where('id', $id)->first();
    //     if($dataChallengeRecord == null){
    //         return [
    //             'result' => 'error',
    //             'message' => 'Challenge tidak ditemukan!',
    //         ];
    //     }
    //     $challengeId = $dataChallengeRecord->challenges_id;
    //     $dataChallenge = Challenge::where('id', $challengeId)->first();
    //     $expChallenge = $dataChallenge->exp;
    //     // dd($expChallenge);
    //     $playerName = $request->CPlayerNameUpdate;
    //     $dataPlayer = Player::where('player_name', $playerName)->first();
    //     if($dataPlayer == null){
    //         return [
    //             'result' => 'error',
    //             'message' => 'Data player tidak ditemukan!',
    //         ];
    //     }
    //     $playerId = $dataPlayer->id;

    //     $update_cr = challenges_record::find($id);
    //     $update_cr->challenges_id = $request->CidUpdate;
    //     $update_cr->t_players_id = $playerId;
    //     $update_cr->status = $statusChallengeString;
    //     $update_cr->save();
    //     if($update_cr == true){
    //         // $player = Player::Where('id', $playerId)->first();
    //         // // dd($player);
    //         // $lastPlayerExp = $player->exp;
    //         // $player->exp = $lastPlayerExp + $exp;
    //         // $player->save();
    //     }
    //     return redirect()->back()->with('success', 'berhasil edit Data quest record!');
    // }
    
}
