<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\Detail_room;
use App\Models\Score;
use App\Models\Game;
use App\Models\Player;
use App\Models\Detail_play;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class RoomController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $room = room::select('id','room_name','t_games_id','t_players_id','max_players','description_room','date')->paginate(10);    
        $cekMember = Detail_room::join('rooms', 'rooms.id', '=', 'detail_rooms.t_rooms_id')
                                ->join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                ->select('players.player_name','detail_rooms.status')
                                ->get(); 
        $playerName = Detail_room::join('players','players.id','=','detail_rooms.t_players_id')
                                ->select('players.player_name')
                                ->get();
        $Scores   = Score::join('games', 'games.id', '=', 'scores.t_games_id')
                            ->join('players', 'players.id', '=', 'scores.t_players_id')
                            ->select('scores.id', 'players.player_name', 'games.game_name', 'scores.score', 'scores.playtime')
                            ->paginate(10);
        $gameName = Detail_room::join('games','games.id','=','detail_rooms.t_games_id')
                                ->join('rooms','rooms.id','=','detail_rooms.t_rooms_id')
                                ->select('games.game_name','detail_rooms.t_rooms_id')
                                ->get();
        return view('pages/page_room', compact('room', 'admin','cekMember','playerName','Scores','gameName'));
    }

    public function listPlayer(Request $request, $id, $roomId)
    {
        $id = Crypt::decryptString($id);
        $roomId = Crypt::decryptString($roomId);
        $admin = Admin::where('id', '=', $id)->first();
        $dataRoom = Room::where('id', $roomId)->first();
        $roomName = $dataRoom->room_name;
        if($roomName == null){
            $roomName = 'Unknown';
        }
        $dataPlayers = Detail_room::join('players', 'players.id', '=', 'detail_rooms.t_players_id')
                                ->where('t_rooms_id', $roomId)
                                ->orderBy('detail_rooms.id')
                                ->select('detail_rooms.id', 'players.player_name', 'players.exp', 'detail_rooms.status')
                                ->paginate(10);
        // dd($dataPlayer);
        return view('pages/page_list_player', compact('dataPlayers', 'admin', 'roomName'));
    }
        

}
