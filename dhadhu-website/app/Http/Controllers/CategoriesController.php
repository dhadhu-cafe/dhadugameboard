<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Admin;
use Illuminate\Support\Facades\Crypt;

class CategoriesController extends Controller
{
    public function index(Request $request, $id)
    {
        $id = Crypt::decryptString($id);
        $admin = Admin::where('id', '=', $id)->first();
        $categories = categories::select('id','category_type','category_name')->paginate(10);
        // dd($admin);
        return view('pages/page_categories', compact('categories', 'admin'));
    }
    public function store(Request $request)
    {
        
        $newCategories = new categories;
        $newCategories->category_type  = $request->CatType;
        $newCategories->category_name  = $request->CatName;
        $newCategories->save();
        if($newCategories){
            return [
                'result' => 'success',
                'message' => 'Berhasil memasukkan data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat memasukkan data!',
            ];
        }
        return redirect()->back();
    }
    public function destroy(Request $request)
    {
        $id = $request->deleteCategoriesId;
        // dd($id);
        $deleteFile = categories::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $TypeCategories = $request->catTypeUpdate;
        // dd($statusChallenge);
        if($TypeCategories == 1){
            $TypeCategoriesString = "Postingan";
        }else if($TypeCategories == 2){
            $TypeCategoriesString = "Menu FnB";
        }elseif($TypeCategories == 3){
            $TypeCategoriesString = "Game";
        }
        // dd($statusChallengeString);
        $update_categories = categories::find($id);
        $update_categories->category_type = $TypeCategoriesString;
        $update_categories->category_name = $request->catNameUpdate;
        $update_categories->save();
        return redirect()->back()->with('success', 'berhasil edit Data kategori!');
    }
}
