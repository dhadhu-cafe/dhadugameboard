<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhadhu Game Board Cafe</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" type="text/css" crossorigin="">
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
    <link href="{{asset('assets/css/style-no4-register.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
    <main>
        <div class="container-register">
            <div class="title">
                <h1>Register</h1>
            </div>
            <!-- alert error -->
            @if(session()->has('error'))
                <div class="alert alert-danger" style="color: red;font-weight:bold">
                    {{ session()->get('error') }}
                </div>
            @endif
            <!-- END::Alert error -->
            
            <form method="POST" action="register/post">
            @csrf
                <div class="form_warp">
                    <div class="input_grp">

                        <div class="input_wrap">
                            <label for="fname">First Name</label>
                            <input type="text" id="fname" name="fname" required>
                        </div>
                        <div class="input_wrap">
                            <label for="lname">Last Name</label>
                            <input type="text" id="lname" name="lname" required>
                        </div>
						
                    </div>

                        <div class="input_wrap">
                            <label for="nick">Nick Name</label>
                            <input type="text" id="nick" name="nickName" required>
                        </div>

                        <div class="input_wrap">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" required>
                        </div>

                        <div class="input_wrap">
                            <label for="Password">Password</label>
                            <input type="password" id="password" name="password" required>
                        </div>

                        <div class="input_wrap">
                            <label for="cfmPassword">Confirm Password</label>
                            <input type="password" id="cfmPassword" name="cfmPassword" required>
                        </div>
         
                        <div class="input_wrap">
                            <label>Gender</label>
                            <ul>
                                <li>
                                    <label class="radio_wrap">
                                        <input type="radio" name="gender" id="" value="male" class="input_radio">
                                        <span>Male</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio_wrap">
                                        <input type="radio" name="gender" id="" value="female" class="input_radio">
                                        <span>Female</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio_wrap">
                                        <input type="radio" name="gender" id="" value="other" class="input_radio">
                                        <span>Other</span>
                                    </label>
                                </li>
                            </ul>
                        </div>

                        <div class="input_wrap">
                            <input type="submit" value="Register Now" class="submit_btn">
                        </div>
         
                </div>
            </form>
        </div>
    </main>

</body>
</html>