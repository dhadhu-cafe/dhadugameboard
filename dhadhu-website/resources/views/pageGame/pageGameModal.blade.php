<!doctype html>
<html lang="en">
<head>
  @include('head')
</head>
<body>

  <main class="container-utama">
    
    <div class="penampung left" id="SideLeft">
      @include('sectionMenuUser.sideNav')
      <span class="btnShow" id="btnMenu">Show Menu</span>
    </div>

    <div class="penampung right" id="SideRight">
<!-------------------------- BEGIN::HEADER -------------------------->
      <div class="stat">
        <div class="tempat-nama">
          @if(isset($user))
          <h5><a href="/profilemanager/{{\Crypt::encryptString($user->id)}}">{{$user->player_name}}</a></h5> <br>
          @else
          <h5><a href="#">sign in</a></h5>
          @endif
          <div class="dropdown-option">
            <a href="youtube.com">Logout</a>
          </div>
        </div>
      </div>
        <!-- alert success -->
        @if(session()->has('success'))
            <div class="alert alert-success" style="color: green;font-weight:bold">
                {{ session()->get('success') }}
            </div>
        @endif
        <!-- END::Alert success -->

        <!-- alert error -->
        @if(session()->has('Error'))
            <div class="alert alert-danger" style="color: red;font-weight:bold">
                {{ session()->get('Error') }}
            </div>
        @endif
        <!-- END::Alert error -->
<!-------------------------- END::HEADER -------------------------->

    </div>
  
  </main>

<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/script-lead.js')}}"></script>
<!-- BEGIN : Bootstrap JS -->

<!-- slick js -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
$('.sub-eventx').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  nextArrow: $(''),
  prevArrow: $(''),
  responsive: [
    {
      breakpoint: 1920,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 300,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>

<script>

function cekRoom(id, playerId){
// alert(id, playerId)
// document.getElementById("loading_image").style.display = "block";
  $.ajax({
    type                : "GET",
    url                 : "/dashboard/"+playerId+"/cek-room",
    data:{
      '_method'         : 'GET',
      'roomId'          : id,
    },
    success: function(data){

        $('.roomMasterCek').html(data['htmlControll'])
        $('.roomDetailCek').html(data['htmlRoomEdit'])

        $('.roomMasterCek').html(data['htmlControllRoomMember'])
        $('.roomDetailCek').html(data['htmlRoomEditMember'])



    },
    complete: function(){
      // document.getElementById("loading_image").style.display = "none";
    }
  })
}

</script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.js" crossorigin=""></script>
<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
</body>
</html>