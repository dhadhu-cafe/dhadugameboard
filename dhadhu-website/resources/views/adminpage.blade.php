<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard Admin</title>

  <!-- Bootstrap core CSS -->
  <link href=" {{asset('assets/css/bootsrap.min.css')}}" rel="stylesheet">
 
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
 
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
  

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
          
          <!-- alert success -->
          @if(session()->has('success'))
              <div class="alert alert-success" style="color: green;font-weight:bold">
                  {{ session()->get('success') }}
              </div>
          @endif
          <!-- END::Alert success -->

          <!-- alert error -->
          <!-- @if(session()->has('Error'))
              <div class="alert alert-danger" style="color: green;font-weight:bold">
                  {{ session()->get('Error') }}
              </div>
          @endif -->
          <!-- END::Alert error -->
          <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
        
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->

    <div id="page-content-wrapper">
          
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="{{route('logoutadmin')}}">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
     <div class="row">
      <div class="col">
        <h2>Dashboard</h2>
        <div class="row">
        <div class="col">
                <div class="small-box bg-info">
                  <div class="inner">
                    <h4>Score</h4>

                    <p>Kelola semua data score pada Dhadhu
                    Cafe</p>
                  </div>
                  <a href="/page_score" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>       
        </div>
        <div class="col">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h4>Postingan</h4>

                    <p>Kelola semua data postingan pada Dhadhu
                    Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_postingan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>      
            </div>
        </div>
        <div class="row">
        <div class="col">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h4>Foods & Beverages</h4>

                    <p>Kelola semua data F&B pada Dhadhu
                    Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_fnb" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h4>Challenge</h4>

                    <p>Kelola semua data challenge pada Dhadhu
                    Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_challenge" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h4>Tambah Kategori</h4>

                    <p>Kelola data kategori pada Dhadhu Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_categories" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h4>Challenge Records</h4>

                    <p>Kelola semua data challenge record pada Dhadhu
                    Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_challenge_record" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h4>Data Player</h4>

                    <p>Kelola data player pada Dhadhu Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_player" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h4>Data Room</h4>

                    <p>Kelola data Room pada Dhadhu Cafe</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            </div>
            <!-- ISIH ONO LE EROR LINK PAGE.E -->
            </div>
           
            <div class="col"><h2>Edit Profil Dhadhu Cafe</h2>
            @foreach($info as $info)
                      <form role="form" method="POST" action="/admin/editinfo/{{$info->id}}">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kontak</label>
                        <input type="text" class="form-control" name="Kontak" id="Kontak" value="{{$info->kontak}}">
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Alamat</label>
                        <textarea class="form-control" id="Alamat" name="Alamat" rows="3" value="{{$info->Alamat}}">{{$info->Alamat}}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jam Buka</label>
                        <br>
                        
                        <div class="row">
                        <div class="col-6">
                        <label for="exampleInputEmail1">Senin</label>
                        <input type="text" class="form-control" name="Senin" id="Senin" value="{{$info->Senin}}">
                        <label for="exampleInputEmail1">Selasa</label>
                        <input type="text" class="form-control" name="Selasa" id="Selasa" value="{{$info->Selasa}}">
                        </div>
                        <div class="col-6">
                        <label for="exampleInputEmail1">Jumat</label>
                        <input type="text" class="form-control"name="Jumat" id="Jumat" value="{{$info->Jumat}}">
                        <label for="exampleInputEmail1">Sabtu</label>
                        <input type="text" class="form-control" name="Sabtu" id="Sabtu" value="{{$info->Sabtu}}">
                        </div>
                        <div class="col-6">
                        <label for="exampleInputEmail1">Rabu</label>
                        <input type="text" class="form-control" name="Rabu" id="Rabu" value="{{$info->Rabu}}">
                        <label for="exampleInputEmail1">Kamis</label>
                        <input type="text" class="form-control" name="Kamis" id="Kamis" value="{{$info->Kamis}}">
                        </div>
                        <div class="col-6">
                        <label for="exampleInputEmail1">Minggu</label>
                        <input type="text" class="form-control" name="Minggu" id="Minggu" value="{{$info->Minggu}}">
                        </div>
                      </div>
                      <br>
                      <button type="submit" class="btn btn-primary" id="updateInfo">Submit</button>                      
                    </form>
                    @endforeach
                    </div>
        

    </div>


    </div>
    </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>

  
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
