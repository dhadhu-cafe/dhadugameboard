<?php $no = 0;?>
@foreach($detailRoomAsMasters as $detailRoomAsMaster)
<?php $no++;?>
<tr>
    <td>{{$no}}</td>
    <td>{{$detailRoomAsMaster->player_name}}</td>
    <td>{{$detailRoomAsMaster->level_player}}</td>
    @if($detailRoomAsMaster->status == 'joined')
    <td>In Room</td>
    @else
    <td>Waiting</td>
    @endif
</tr>
@endforeach

