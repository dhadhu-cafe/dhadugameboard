
    <div class="room-control right">
        @if($roomMaster == true)
        <div class="room-section-edit">
            <div class="title-room-control">
                <p>Edit Room</p>
            </div>
            
            <div class="split-room-section">
                <div class="content-split-room">
                    <label for="">Room Name</label> <br>
                    <input type="text" name="" id="" placeholder="Room Name" value="{{$controllRoom->room_name}}"> <br>
                    <input class="hidden display: none" type="hidden" name="" id="" placeholder="Room Name" value="{{$controllRoom->id}}">
                </div>
                
                <div class="content-split-room">
                    <label for="">Game Name</label><br>
                    <input type="text" name="" id="" placeholder="Game Name" value="{{$controllRoom->game_name}}"> <br>
                </div>
            </div>
            
            <label for="">Description</label><br>
            <input type="text" name="" id="" placeholder="Description" value="{{$controllRoom->description_room}}"> <br>
            
            <label for="">Max Player</label> <br>
            <input type="number" name="" id="" placeholder="Max Player" value="{{$controllRoom->max_players}}"> <br>

            <label for="">Date Play</label> <br>
            <input type="date" name="" id="" value="{{$controllRoom->date}}"> <br>
        
            <button>Edit</button> 
            <form method="POST" action="/dashboard/destroy-room/{{$controllRoom->id}}">
            @method('DELETE')
            @csrf
            <button type="submit">Bubarkan Room</button>
            </form>
        </div>
        @elseif($joinRoom == true)
        <div class="room-section-edit">
            <div class="title-room-control">
                <p>Edit Room</p>
            </div>
            
            <label for="">Room Name</label> <br>
            <input type="text" name="" id="" placeholder="Room Name" value="{{$controllRoomForMember->room_name}}" readonly> <br>
            <input class="hidden display: none" type="hidden" name="" id="" placeholder="Room Name" value="{{$controllRoomForMember->id}}" readonly>

            <label for="">Game Name</label><br>
            <input type="text" name="" id="" placeholder="Game Name" value="{{$controllRoomForMember->game_name}}" readonly> <br>

            <label for="">Description</label><br>
            <input type="text" name="" id="" placeholder="Description" value="{{$controllRoomForMember->description_room}}" readonly> <br>

            <label for="">Max Player</label> <br>
            <input type="text" name="" id="" placeholder="Max Player" value="{{$controllRoomForMember->max_players}}" readonly> <br>

            <label for="">Date Play</label> <br>
            <input type="date" name="" id="" value="{{$controllRoomForMember->date}}" readonly> <br>
            
            <form method="POST" action="/dashboard/leave-room/{{$controllRoomForMember->t_players_id}}">
            <input type="hidden" name="roomId" value="{{$controllRoomForMember->id}}">
            @method('DELETE')
            @csrf
            <button type="submit">Tinggalkan Room</button>
            </form>
        </div>
        @endif
    </div>