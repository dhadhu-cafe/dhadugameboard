@foreach($games as $game)
<div class="container-event-game">
    <div class="pas-foto">
        <img src="{{asset('gambar/'.$game->image_game) }}" alt="PasFoto" onclick="showGamex({{$game->id}})">
    </div>

    <div class="container-nama-harga">
        <h5>{{$game->game_name}}</h5>
        <p>{{$game->difficulty_game}}</p>
    </div>  
</div>
@endforeach