<!doctype html>
<html lang="en">
<head>
  @include('head')
</head>
<body>

  <main class="container-utama">
    <!-- Modal -->
    @include('modalView.dashboard-modal')
    @include('modalView.fnb-modal')
    @include('modalView.promo&more-modal')
    @include('modalView.roller-game-modal')
    @include('modalView.roller-game-g')
    @include('Filter.FilterPromo')
    @include('Filter.FilterGame')
    <!-- Modal -->
    <div class="penampung left" id="SideLeft">
      @include('sectionMenuUser.sideNav')
      <span class="btnShow" id="btnMenu">Show Menu</span>
    </div>

    <div class="penampung right" id="SideRight">
<!-------------------------- BEGIN::HEADER -------------------------->
      <div class="stat">
        <div class="tempat-nama">
          <div class="name">
            @if(isset($user))
              @if($user->player_name != null)
              <h5>{{Str::limit($user->player_name, 8)}}</h5>
              <h5 style="float: right">{{$user->exp}}</h5>
              @else
              <h5></h5>
              @endif
            @else
              <h5>Guest</h5>
              <h5>0</h5>
            @endif
          </div>
          @if(isset($user))
            @if($user->player_image != null)
            <img src="{{asset('gambar/'.$user->player_image)}}" alt="Avatar" class="image-avatar" onclick="AvaPP()">
            @else
            <img src="{{asset('assets/foto/img_avatar2.png')}}" alt="Avatar" class="image-avatar" onclick="AvaPP()">
            @endif
          @else
          <h5><a href="#">sign in</a></h5>
          @endif
          <div class="dropdown-option" id="ProfPP">
            <a href="/profilemanager/{{($user->id)}}">Profile</a> <br>
            <a href="{{route('logoutuser')}}">Logout</a> <br>
          </div>
        </div>
      </div>

<!-------------------------- END::HEADER -------------------------->

<!-------------------------- BEGIN::DASHBOARD -------------------------->

        <section class="mysection active">
          @include('sectionMenuUser.dashboard')
        </section>

<!-------------------------- END::DASHBOARD -------------------------->

<!-------------------------- BEGIN::FOODS&DRINKS -------------------------->

        <section class="mysection">
          <div class="eventx">
          @include('sectionMenuUser.fnb')
          </div> 
        <!-- modal -->
        
        <!-- modal -->  
        </section>

<!-------------------------- END::FOODS&DRINKS -------------------------->

<!-------------------------- BEGIN::PROMOS&MORES -------------------------->

        <section class="mysection">
          <div style="padding: 5px">
            <div class="filter-promo-style">
                <h2>Promo & More!</h2>  
                <div class="filter-promo">
                    <div class="span-filter" id="ShowFilter" onclick="FilterShow()"><i class="fas fa-filter"></i>Filter</div>
                </div>
            </div>
          </div>

          <div class="three-col">
          <center><img src="http://i.stack.imgur.com/FhHRx.gif" alt="" id="loading_image" style="display: none; top: 200px; left: 50%; position: absolute;"></center>
            <!-- Start -->
            <div id="eventsRender">

            @foreach($events as $event)
              <img class="pas-foto" src="{{asset('gambar/'.$event->image_event)}}" onclick="showPnM({{$event->id}})" >
              <div class="name-space">
                <h5>{{$event->event_name}}</h5>
                <h5>{{$event->category}}</h5>
              </div>
            @endforeach

            </div>
            <!-- End -->
          </div>
        </section>

<!-------------------------- END::PROMOS&MORES -------------------------->

<!-------------------------- BEGIN::GAMES -------------------------->

        <section class="mysection">
        
          @include('sectionMenuUser.roller-games')
          <!-- Modal -->
          @include('modalView.roller-game-modal')
          <!-- Modal -->
        </section>

<!-------------------------- END::GAMES -------------------------->

<!-------------------------- BEGIN::Plays -------------------------->

      <section class="mysection">
        <div class="container-challenge">
          <h1>Playground</h1>
            @if($roomMaster == true)
            <div class="container-challenge-navbar">
              <div class="challenge-navbar-menu" id="DivPlay">
                <div class="nav-play turnOn" id="roomControl1">Room Control</div> 
                <div class="nav-play " id="roomControl2">All Upcoming Plays</div> 
              </div>
            </div>
            @elseif($joinRoom == true)
            <div class="container-challenge-navbar">
              <div class="challenge-navbar-menu" id="DivPlay">
                <div class="nav-play turnOn" id="roomControl1">Room Control</div> 
                <div class="nav-play " id="roomControl2">All Upcoming Plays</div> 
                <div class="nav-play ">Room</div>
              </div>
            </div>
            @else
            <div class="container-challenge-navbar">
              <div class="challenge-navbar-menu" id="DivPlay">
                <div class="nav-play turnOn" style="display: none"></div> 
                <div class="nav-play ">All Upcoming Plays</div> 
                <div class="nav-play ">Room</div> 
              </div>
            </div>
            @endif

          @include('sectionMenuUser.roller-play')

        </div>
      </section>

<!-------------------------- END::Plays -------------------------->

<!-------------------------- BEGIN::Leaderboard -------------------------->

        <section class="mysection">
          <div class="container-leaderboard"> 

            @include('sectionMenuUser.roller-leaderboard')

          </div>
        </section>

<!-------------------------- END::Leaderboard -------------------------->

<!-------------------------- BEGIN::Challenges -------------------------->

        <section class="mysection">
          <div class="container-challenge">
            <h1>Challenge</h1>
            <h5>Everytime you completed a quest, tell the NPC (Game Master) so you can level up!</h5>
              <!-- Start -->
              <hr>
              <h2>Challenge Yang Tersedia</h2>
              @foreach($challenges as $challenge)
              <div class="container-quest ">
                <div class="container-name-quest left grandient-second">
                  <p>{{$challenge->challenge_name}}</p>
                </div>
                <div class="container-name-quest center grandient-second">
                  <p>{{$challenge->description_challenge}}</p>
                </div>
                <div class="container-name-quest right grandient-second">
                <p>{{$challenge->exp}} Exp</p>
                </div>
              </div>
              @endforeach
              <!-- End -->

              <hr>
              <h2>Challenge Yang Sudah Selesai</h2>
              <!-- Start -->
              @if(count($statusChallenges))
              @foreach($statusChallenges as $statusChallenge)
              <div class="container-quest ">
                <div class="container-name-quest left grandient-first">
                  <p>{{$statusChallenge->challenge_name}}</p>
                </div>
                <div class="container-name-quest center grandient-first">
                  <p>{{$statusChallenge->description_challenge}}</p>
                </div>
                <div class="container-name-quest right grandient-first">
                  <p>{{$statusChallenge->status}}</p>
                </div>
              </div>
              @endforeach
              @else
                <h5>Belum ada quest yang anda selesaikan! ayo lebih semangat lagi!</h5>
              @endif
              <!-- End -->
          </div>
        </section>

<!-------------------------- END::Challenges  -------------------------->

<!-------------------------- START::Info  -------------------------->

        <section class="mysection">
          @include('sectionMenuUser.info')
        </section>

<!-------------------------- END::INFO -------------------------->
    </div>
  
  </main>

<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/script-lead.js')}}"></script>
<!-- BEGIN : Bootstrap JS -->

<!-- slick js -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
$('.sub-eventx').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  nextArrow: $(''),
  prevArrow: $(''),
  responsive: [
    {
      breakpoint: 1920,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 1366,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 720,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 300,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>

<script>

function cekRoom(id, playerId){

// alert(id, playerId)
// document.getElementById("loading_image").style.display = "block";
  $.ajax({
    type                : "GET",
    url                 : "/dashboard/"+playerId+"/cek-room",
    data:{
      '_method'         : 'GET',
      'roomId'          : id,
    },
    success: function(data){

        $('.roomMasterCek').html(data['htmlControll'])
        $('.roomDetailCek').html(data['htmlRoomEdit'])

        $('.roomMasterCek').html(data['htmlControllRoomMember'])
        $('.roomDetailCek').html(data['htmlRoomEditMember'])




    },
    complete: function(){
      // document.getElementById("loading_image").style.display = "block";



    }
  })
}

</script>

<script>
  
</script>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.js" crossorigin=""></script>
<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  @include('sweetalert::alert')
  <!-- SWEET ALERT -->
</body>
</html>