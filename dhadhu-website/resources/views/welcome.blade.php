<!doctype html>
<html lang="en">
<head>
  @include('head')
</head>
<body>

  <main class="container-utama">
    <!-- modal -->
      @include('modalView.dashboard-modal')
      @include('modalView.fnb-modal')
      @include('modalView.promo&more-modal')
      @include('modalView.roller-game-modal')
      @include('modalView.roller-game-g')
      @include('Filter.FilterPromo')
      @include('Filter.FilterGame')
    <!-- modal -->
      

    <div class="penampung left" id="SideLeft">
      <img src="{{asset('assets/css/Logo-Dhadhu.png')}}" alt="Logo">
      
      <ul class="first-ul" id="myDiv">
      <!-- alert error -->
      @if(session()->has('success'))
          <div class="alert alert-success" style="color: green;font-weight:bold">
              {{ session()->get('success') }}
          </div>
      @endif
      <!-- END::Alert error -->
          <li><a href="#" class="nav-link nyala">Dashboard</a></li>
          <li><a href="#" class="nav-link">Foods & Drinks</a></li>
          <li><a href="#" class="nav-link">Promo & More</a></li>
          <li><span onclick="coba()" class="menu">Roller</span>
            <ul id="myDropdown" class="dropdown-content">
              <li><a href="#" class="nav-link">Games</a></li>
              <li><a href="#" class="nav-link">Play</a></li>
              <li><a href="#" class="nav-link">Leaderboard</a></li>
            </ul>
          </li>
          <li><a href="#" class="nav-link">Challenge</a></li>
          <li><a href="#" class="nav-link">Info</a></li>
      </ul>
      <span class="btnShow" id="btnMenu">Show Menu</span>
    </div>

    <div class="penampung right" id="SideRight">
<!-------------------------- BEGIN::HEADER -------------------------->
      <div class="stat">
        @include('header')
      </div>
<!-------------------------- END::HEADER -------------------------->

<!-------------------------- BEGIN::DASHBOARD -------------------------->

      <!-- BEGIN:: EVENT IN DASHBOARD -->
        <section class="mysection active">
          @include('sectionMenuUser.dashboard')         
        </section>
<!-------------------------- END::DASHBOARD -------------------------->

<!-------------------------- BEGIN::FOODS&DRINKS -------------------------->

        <section class="mysection">
          <div class="eventx">
          @include('sectionMenuUser.fnb')
          </div> 
        </section>

<!-------------------------- END::FOODS&DRINKS -------------------------->

<!-------------------------- BEGIN::PROMOS&MORES -------------------------->

        <section class="mysection">
          
          <div style="padding: 5px">
            <div class="filter-promo-style">
                <h2>Promo & More!</h2>  
                <div class="filter-promo">
                    <div class="span-filter" id="ShowFilter" onclick="FilterShow()"><i class="fas fa-filter"></i>Filter</div>
                </div>
            </div>
          </div>

          <div class="three-col">
            
            <!-- Start -->
            <div id="eventsRender2">
            @foreach($events as $event)
            <a href="#pnmModal-{{$event->id}}">
              <img class="pas-foto" src="{{asset('gambar/'.$event->image_event)}}" onclick="showPnM({{$event->id}})" >
            </a>
              <div class="name-space">
                <h5>{{$event->event_name}}</h5>
                <h5>{{$event->category}}</h5>
              </div>
            @endforeach
            </div>
            <!-- End -->
          </div>
          @include('modalView.promo&more-modal')
        </section>

<!-------------------------- END::PROMOS&MORES -------------------------->

<!-------------------------- BEGIN::GAMES -------------------------->

      <section class="mysection">  
        @include('sectionMenuUser.roller-games')
      </section>

<!-------------------------- END::GAMES -------------------------->

<!-------------------------- BEGIN::Plays -------------------------->

        <section class="mysection">
          @include('sectionMenuUser.roller-play-guest')
        </section>

<!-------------------------- BEGIN::Plays -------------------------->

<!-------------------------- BEGIN::Leaderboard -------------------------->

        <section class="mysection">
          <div class="container-leaderboard"> 
            @include('sectionMenuUser.roller-leaderboard')
          </div>
        </section>

<!-------------------------- BEGIN::Challenge -------------------------->

        <section class="mysection">
          <div class="container-challenge">
            <h1>Challenge</h1>
            <h5>Everytime you completed a quest, tell the NPC (Game Master) so you can level up!</h5>
              <!-- Start -->
              <hr>
              <h2>Challenge Yang Tersedia</h2>
              @foreach($challenges as $challenge)
              <div class="container-quest ">
                <div class="container-name-quest left grandient-second">
                  <p>{{$challenge->challenge_name}}</p>
                </div>
                <div class="container-name-quest center grandient-second">
                  <p>{{$challenge->description_challenge}}</p>
                </div>
                <div class="container-name-quest right grandient-second">
                  <p>Belum Selesai</p>
                </div>
              </div>
              @endforeach
              <!-- End -->
          </div>
        </section>

<!-------------------------- END::Challenge -------------------------->

<!-------------------------- BEGIN::INFO -------------------------->

        <section class="mysection">
        @include('sectionMenuUser.info')
        </section>

<!-------------------------- END::INFO -------------------------->
    </div>
  
  </main>

<script type="text/javascript" src="{{asset('assets/js/script.js')}}">


</script>
<!-- BEGIN : Bootstrap JS -->

<!-- slick js -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
$('.sub-eventx').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  nextArrow: $(''),
  prevArrow: $(''),
  responsive: [
    {
      breakpoint: 1920,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
</script>

<script>
function multipleFilters(){
    // var csrfToken         = $('meta[name="csrf-token"]').attr('content');
      var selectedMonth = $("input[name='selectRow[]']:checked").map(function(){
      return $(this).val();
      }).get();      
      var selectedEvent = $("input[name='selectRowEvent[]']:checked").map(function(){
      return $(this).val();
      }).get();

      document.getElementById("loading_image").style.display = "block";
      $.ajax({
        type                : "GET",
        url                 : "/dashboard/guest/multiFilterEvent",
        data:{
          '_method'         : 'GET',
          // '_token'          : csrfToken,
          'months'          : selectedMonth,
          'events'          : selectedEvent,
        },
        success: function(data){
          $('#eventsRender2').html(data['htmlFilterEvents'])
        },
        complete: function(){
          document.getElementById("loading_image").style.display = "none";
        }
      })
  }
</script>

<script>
function multipleFiltersGame(){

var searchData = $("input[name='searchGames']").map(function(){
    return $(this).val();
    }).get();

var selectRowDifficulty = $("input[name='selectRowDifficulty']:checked").map(function(){
    return $(this).val();
    }).get();
var selectRowMaxp = $("input[name='selectRowMaxp']:checked").map(function(){
    return $(this).val();
    }).get();
var selectRowDur = $("input[name='selectRowDur']:checked").map(function(){
    return $(this).val();
    }).get();
var selectRowAge = $("input[name='selectRowAge']:checked").map(function(){
    return $(this).val();
    }).get();

  document.getElementById("loading_image").style.display = "block";
  $.ajax({
      type                : "GET",
      url                 : "/dashboard/guest/multiFilterGame",
      data:{
      '_method'         : 'GET',
      // '_token'          : csrfToken,   
      'search'                : searchData,
      'selectRowDifficulty'   : selectRowDifficulty,
      'selectRowMaxp'         : selectRowMaxp,
      'selectRowDur'          : selectRowDur,
      'selectRowAge'          : selectRowAge,
      },

      success: function(data){
      $('#componentFilterGame').html(data['htmlFilterGames'])
      },
      complete: function(){
      document.getElementById("loading_image").style.display = "none";
      }
  })
}
</script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.js" crossorigin=""></script>
<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
</body>
</html>