<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard Admin Room</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href=" {{asset('assets/css/bootsrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="{{route('logoutadmin')}}">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Room</h3>
              </div>
            </div>
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nama Room</th>
                            <th>Id Game</th>
                            <th>Id Player</th>
                            <th>Player Maksimal</th>
                            <th>Deskripsi Room</th>
                            <th>Tanggal</th>
                        </tr> 
                      </thead>
                      <tbody>
                              <?php $no = 0;?>
                              @foreach($room as $p)
                              <?php $no++;?>
                              <tr>
                                <td align="center">
                                <a type="button " class="btn btn-info " href="/list_player/{{\Crypt::encryptString($admin->id)}}/{{\Crypt::encryptString($p->id)}}" ><em class="fa fa-info"></em></a>
                                <button class="btn btn-success " data-toggle="modal" data-target="#modalAddScore{{$p->id}}"><em class="fa fa-pencil"> </em></a>   
                                </td>
                                <td class="hidden-xs">{{$p->id}}</td>
                                <td>{{$p->room_name}}</td>
                                <td>{{$p->t_games_id}}</td>
                                <td>{{$p->t_players_id}}</td>
                                <td>{{$p->max_players}}</td>
                                <td>{{$p->description_room}}</td>
                                <td>{{$p->date}}</td>
                              </tr>
                              @endforeach
                        </tbody>
                    </table>

                  {{$room->Links()}}
                </div>
         
                            <!-- MODAL LIST PLAYER -->
                             @foreach($room as $p)
                                <div id="modalUpdateroom{{$p->id}}" tabindex="-3"class="modal fade">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4>Daftar Player</h4>
                                      </div>
                                      <div class ="modal-body">
                                      <table class="table table-striped table-bordered table-list">
                                        <thead>
                                          <tr>
                                              <th>Nickname Player</th>
                                              <th>Status</th>
                                          </tr> 
                                        </thead>
                                        <tbody>
                                            @foreach($cekMember as $cek)
                                            <tr>
                                            <td>{{$cek->player_name}}</td>
                                            @if($cek->status == 'joined')
                                                <td>In Room</td>
                                                @else
                                                <td>Waiting</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                      </table>  
                                      </div>
                                    </div>
                                    <!-- modal content end -->
                                  </div> 
                                  <!-- modal dialog end -->
                                </div>
                                @endforeach
            </div><!--panel -->
          </div><!--col 12 -->
        </div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->

  <!-- begin::alert -->
  @include('sweetalert::alert')
  <!-- End::alert -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
  
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
<!-- BEGIN JS : SHOW AND HIDE PASSWORD TAMBAH DATA -->
<script>
$(document).ready(function(){
    $('#checkbox').on('change', function(){
        $('#password').attr('type',$('#checkbox').prop('checked')==true?"text":"password"); 
    });
});
</script>
<!-- END -->
<!-- BEGIN JS : SHOW AND HIDE PASSWORD TAMBAH DATA -->
<script>
$(document).ready(function(){
    $('#checkboxUpdate').on('change', function(){
        $('#PasswordUpdate').attr('type',$('#checkboxUpdate').prop('checked')==true?"text":"password"); 
    });
});
</script>
<!-- END -->

   <!-- BEGIN JS::INSERT DATA / STORE -->
   <script>
    $(document).on('click', '.insertData', function(){
      var csrfToken     = $('meta[name="csrf-token"]').attr('content');
      var gameId        = $(this).closest("div").find('#gameName').val();
      var playerId      = $(this).closest("div").find('#nickplayer').val();
      var score    = $(this).closest("div").find('#TotalScore').val();
      var playtime      = $(this).closest("div").find('#time').val();
      // alert(gameId, playerId, totalScore, playtime)
      Swal.fire({
        title               : 'Apakah anda yakin?',
        text                : "Data akan masuk ke dalam Database!",
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        confirmButtonText   : 'Ya!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type            : "POST",
            url             : "/page_room/post",
            data:{
              '_method'     : 'POST',
              '_token'      : csrfToken,
              'game_name'   : gameId,
              'player_name' : playerId,
              'score'       : score,
              'playtime'    : playtime,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
    </script>
  <!-- END JS::INSERT DATA / STORE -->

  <script>
    $(document).on('click', '.deletePlayer', function(){
      var csrfToken           = $('meta[name="csrf-token"]').attr('content');
      var deletePlayerId   = $(this).closest("td").find('.deletePlayerId').val();
      alert(deletePlayerId)
      Swal.fire({
        title               : 'Apakah anda yakin ingin menghapus file?',
        text                : 'Data akan di hapus dari Database!',
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#d33',
        cancelButtonColor   : '#3085d6',
        confirmButtonText   : 'Delete!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type                   : "DELETE",
            url                    : "/page_player/delete/{id}",
            data:{
              '_method'            : 'DELETE',
              '_token'             : csrfToken,
              'deletePlayerId'  : deletePlayerId,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
  </script>
</body>

</html>
