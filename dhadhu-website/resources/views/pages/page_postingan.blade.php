<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard Admin Postingan</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
    <!-- summernote css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote.min.css" integrity="sha256-n3YISYddrZmGqrUgvpa3xzwZwcvvyaZco0PdOyUKA18=" crossorigin="anonymous" />
  <!-- end summernote css -->
</head>
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Postingan</h3>
              </div>
              <div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>

            <div id="ModalExample" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Postingan</h4>
                  </div>
                  <div class ="modal-body">

                    <form role="form" method="POST" action="/page_postingan/post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="eventName">Judul Post</label>
                        <input type="text" class="form-control" name="eventName" id="eventName" placeholder="Masukan Judul Postingan ">
                      </div>
                      <div class="form-group">
                        <label for="evenCat">Kategori Post</label>
                       
                        <select name="categoryPost"  id="categoryPost"class="form-control">
                        @foreach($categoriesPost as $c)
                          <option value="{{$c->category_name}}">{{$c->category_name}}</option>
                        @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                        <label for="eventDesc">Isi Post</label>
                        <br>
                        <textarea class="summernote" name="descriptionPost" id="descriptionPost"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input type="file" class="form-control" id="imagePost" name="imagePost" required>
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>                      
                    </form>
                    
                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
              <table class="table table-striped table-bordered table-list">
                <thead>
                  <tr>
                      <th><em class="fa fa-cog"></em></th>
                      <th class="hidden-xs">ID</th>
                      <th>Judul Postingan</th>
                      <th>Kategori Postingan</th>
                      <th>Isi Postingan</th>
                      <th>Gambar Postingan</th>
                      
                  </tr> 
                </thead>
                <tbody>
                  @foreach($events as $event)
                  <tr>
                    <td align="center">
                      <a class="btn btn-default"data-toggle="modal" data-target="#modalUpdatePostingan{{$event->id}}"><em class="fa fa-pencil"></em></a>
                      <button class="btn btn-danger deletePostingan deletePostinganId" value="{{$event->id}}"><em class="fa fa-trash"></em></button>
                    </td>
                    <td class="hidden-xs">{{$event->id}}</td>
                    <td>{{$event->event_name}}</td>
                    <td>{{$event->category}}</td>
                    <td>{!!$event->description_event !!}</td>
                    <td><img src="{{asset('/gambar/'.$event->image_event)}}"class="img-thumbnail" width="75"></td>
                  </tr>
                        <!-- EDIT MODAL -->
                    <div id="modalUpdatePostingan{{$event->id}}" class="modal fade">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4>Edit Data Postingan</h4>
                        </div>
                        <div class ="modal-body">
                          <form role="form" method="POST" action="/page_postingan/edit/{{\Crypt::encryptString($event->id)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                              <label for="eventName">Judul Post</label>
                              <input type="text" class="form-control" name="titlePost" id="" value="{{$event->event_name}}">
                            </div>
                            <div class="form-group">
                              <label for="evenCat">Kategori Post</label>
                              <select name="categoryPost"  id="categoryPost"class="form-control">
                                <option selected="" value="{{$event->category}}">{{$event->category}}</option>
                                @foreach($categoriesPost as $c)
                                <option value="{{$c->category_name}}">{{$c->category_name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="eventDesc">Isi Post</label>
                              <br>
                              <textarea class="summernote" name="descriptionPost" id="">{!!$event->description_event!!}</textarea>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Gambar</label>
                              <div>
                                <input type="hidden" name="prevImage" value="{{$event->image_event}}">
                                <img src="{{asset('/gambar/'.$event->image_event) }}"class="img-thumbnail" width="75" height="75">
                              </div>
                              <input type="file" class="form-control" id="imagePost" name="imagePost" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>                      
                          </form>
                          
                        </div>
                      </div>
                      <!-- modal content end -->
                    </div> 
                    <!-- modal dialog end -->
                  </div>
                  @endforeach
                </tbody>
              </table>
              {{$events -> links()}}
            <div class="panel-footer">
                    
          </div>
        </div>
    </div><!--panel -->
  </div><!--col 12 -->
</div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->
  
  <!-- iki ojo di inlangi, tapi sumer note e tok -->
    @include('sweetalert::alert')
  <!-- iki ojo di inlangi, tapi sumer note e tok -->

   <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->
  <!-- summernote script-->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

  <!-- END SUMMERNOTE SCRIPT -->

 <!-- SWEET ALERT -->
 <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->

 <!-- summernote script-->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $('.summernote').summernote();
  });
</script>
<script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
<!-- END SUMMERNOTE SCRIPT -->

    </script>
  <!-- JS::INSERT DATA / STORE -->


  <script>
    $(document).on('click', '.deletePostingan', function(){
      var csrfToken           = $('meta[name="csrf-token"]').attr('content');
      var deletePostinganId   = $(this).closest("td").find('.deletePostinganId').val();
      alert(deletePostinganId)
      Swal.fire({
        title               : 'Apakah anda yakin ingin menghapus file?',
        text                : 'Data akan di hapus dari Database!',
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#d33',
        cancelButtonColor   : '#3085d6',
        confirmButtonText   : 'Delete!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type                   : "DELETE",
            url                    : "/page_postingan/delete/{id}",
            data:{
              '_method'            : 'DELETE',
              '_token'             : csrfToken,
              'deletePostinganId'  : deletePostinganId,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
  </script>
</body>

</html>