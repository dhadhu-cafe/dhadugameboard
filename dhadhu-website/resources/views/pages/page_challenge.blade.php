<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard Admin Challenge</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Challenge</h3>
              </div>
              <div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>

            <div id="ModalExample" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Challenge</h4>
                  </div>
                  <div class ="modal-body">

                    <form role="form" method="POST">
                      @csrf
                      <div class="form-group">
                        <label for="chalname">Nama Challenge</label>
                        <input type="text" class="form-control" id="chalname" placeholder="Masukan Nama ">
                      </div>
                      <div class="form-group">
                        <label for="chaldesc">Deskripsi Challenge</label>
                        <input type="text" class="form-control" id="chaldesc" placeholder="Masukan Deskripsi ">
                      </div>
                      <div class="form-group">
                        <label for="chaldesc">Exp</label>
                        <input type="number" class="form-control" id="chalexp" placeholder="Masukan Exp ">
                      </div>
                      <button type="button" class="btn btn-primary insertchallenge">Submit</button>                      
                    </form>

                    
                    
                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nama Challenge</th>
                            <th>Deskripsi Challenge</th>
                            <th>Exp</th>
                        </tr> 
                      </thead>
                      <tbody>
                              @foreach($challenge as $c)
                              <tr>
                                <td align="center">
                                  <a class="btn btn-default" data-toggle="modal" data-target="#modalUpdateChallenges{{$c->id}}"><em class="fa fa-pencil"></em></a>
                                  <button class="btn btn-danger deleteChallenge deleteChallengeId" value="{{$c->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$c->id}}</td>
                                <td>{{$c->challenge_name}}</td>
                                <td>{{$c->description_challenge}}</td>
                                <td>{{$c->exp}}</td>
                              </tr>
                              <!-- MODAL EDIT DATA -->
                              <div id="modalUpdateChallenges{{$c->id}}" tabindex="1"class="modal fade">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4>Edit Data Challenge</h4>
                                      </div>
                                      <div class ="modal-body">
                                        <!-- FORM UPDATE DATA -->
                                        <form action="/page_challenge/edit/{{\Crypt::encryptString($c->id)}}" method="POST">
                                          @csrf
                                          <div class="form-group">
                                            <label for="CPlayerid">Id Player</label>
                                            <input type="text" class="form-control" name="CNameUpdate" id="CNameUpdate" value="{{$c->challenge_name}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Id Player</label>
                                            <input type="text" class="form-control" name="CDescUpdate" id="CDescUpdate" value="{{$c->description_challenge}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Id Player</label>
                                            <input type="number" class="form-control" name="CExpUpdate" id="CExpUpdate" value="{{$c->exp}}">
                                          </div>

                                          <button type="submit" class="btn btn-primary">Submit</button>     
                                        </form>   
                                      </div>
                                    </div>
                                    <!-- modal content end -->
                                  </div> 
                                  <!-- modal dialog end -->
                              @endforeach   
                        </tbody>
                    </table>               
                  {{$challenge->links()}}
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->

  <!-- begin::alert -->
  @include('sweetalert::alert')
  <!-- End::alert -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
  
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <!-- BEGIN JS::INSERT DATA / STORE -->
    <script>
    $(document).on('click', '.insertchallenge', function(){
      var csrfToken     = $('meta[name="csrf-token"]').attr('content');
      var chalname      = $(this).closest("div").find('#chalname').val();
      var chaldesc      = $(this).closest("div").find('#chaldesc').val();
      var chalexp       = $(this).closest("div").find('#chalexp').val();
    
      Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data akan masuk ke dalam Database!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: "POST",
            url: "/page_challenge/post",
            data:{
              '_method'      : 'POST',
              '_token'       : csrfToken,
              'chalname'     : chalname,
              'chaldesc'     : chaldesc,
              'chalexp'     : chalexp,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Inputed!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
    </script>
  <!-- END JS::INSERT DATA / STORE -->

  <script>
    $(document).on('click', '.deleteChallenge', function(){
      var csrfToken           = $('meta[name="csrf-token"]').attr('content');
      var deleteChallengeId   = $(this).closest("td").find('.deleteChallengeId').val();
      alert(deleteChallengeId)
      Swal.fire({
        title               : 'Apakah anda yakin ingin menghapus file?',
        text                : 'Data akan di hapus dari Database!',
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#d33',
        cancelButtonColor   : '#3085d6',
        confirmButtonText   : 'Delete!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type                   : "DELETE",
            url                    : "/page_challenge/delete/{id}",
            data:{
              '_method'            : 'DELETE',
              '_token'             : csrfToken,
              'deleteChallengeId'  : deleteChallengeId,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
  </script>
</body>

</html>
