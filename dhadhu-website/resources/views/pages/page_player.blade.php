<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard Admin Player</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href=" {{asset('assets/css/bootsrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Player</h3>
              </div>
            </div>
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nickname</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Gender</th>
                            <th>Exp</th>
                        </tr> 
                      </thead>
                      <tbody>
                              <?php $no = 0;?>
                              @foreach($player as $p)
                              <?php $no++;?>
                              <tr>
                                <td align="center">
                                <button class="btn btn-info " data-toggle="modal" data-target="#modalUpdateCategories{{$p->id}}"><em class="fa fa-pencil"></em></a>
                                  <button class="btn btn-danger deletePlayer deletePlayerId" value="{{$p->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$p->id}}</td>
                                <td>{{$p->player_name}}</td>
                                <td>{{$p->fname}}</td>
                                <td>{{$p->lname}}</td>
                                <td>{{$p->email}}</td>
                                <td>{{$p->password}}</td>
                                <td>{{$p->gender}}</td>
                                <td>{{$p->exp}}</td>
                              </tr>
                              
        <!-- MODAL EDIT DATA -->
                              <div id="modalUpdateCategories{{$p->id}}" tabindex="1"class="modal fade">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4>Edit Data Player</h4>
                                      </div>
                                      <div class ="modal-body">
                                      <!-- FORM UPDATE DATA -->
                                        <form action="/page_player/edit/{{\Crypt::encryptString($p->id)}}" method="POST">
                                          @csrf
                                          <div class="form-group">
                                            <label for="CPlayerid">Nickname</label>
                                            <input type="text" class="form-control" name="NicknameUpdate" id="NicknameUpdate" value="{{$p->player_name}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">First Name</label>
                                            <input type="text" class="form-control" name="FNameUpdate" id="FNameUpdate" value="{{$p->fname}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Last name</label>
                                            <input type="text" class="form-control" name="LNameUpdate" id="LNameUpdate" value="{{$p->lname}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Email</label>
                                            <input type="text" class="form-control" name="EmailUpdate" id="EmailUpdate" value="{{$p->email}}">
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Password</label>
                                            <input type="password" class="form-control" name="PasswordUpdate" id="PasswordUpdate" value="{{$p->password}}">
                                            <input type="checkbox" id="checkboxUpdate"> Show Password
                                          </div>
                                          <div class="form-group">
                                            <label for="Cid">Gender</label>
                                            <select class="browser-default custom-select" name="GenderUpdate" id="GenderUpdate" value="{{$p->category_type}}">
                                                @if($p->gender == "male")
                                                <option selected value="1">male</option>
                                                <option value="2">female</option>>
                                                @elseif($p->gender=="female")
                                                <option value="1">male</option>
                                                <option selected=""value="2">female</option>
                                                @endif
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label for="CPlayerid">Level Player</label>
                                            <input type="text" class="form-control" name="LevelUpdate" id="LevelUpdate" value="{{$p->exp}}">
                                          </div>
                                          <button type="submit" class="btn btn-primary">Submit</button>     
                                        </form>   
                                      </div>
                                    </div>
                                    <!-- modal content end -->
                                  </div> 
                                  <!-- modal dialog end -->
                              @endforeach
                        </tbody>
                    </table>

                  {{$player->Links()}}
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->

  <!-- begin::alert -->
  @include('sweetalert::alert')
  <!-- End::alert -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
  
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
<!-- BEGIN JS : SHOW AND HIDE PASSWORD TAMBAH DATA -->
<script>
$(document).ready(function(){
    $('#checkbox').on('change', function(){
        $('#password').attr('type',$('#checkbox').prop('checked')==true?"text":"password"); 
    });
});
</script>
<!-- END -->
<!-- BEGIN JS : SHOW AND HIDE PASSWORD TAMBAH DATA -->
<script>
$(document).ready(function(){
    $('#checkboxUpdate').on('change', function(){
        $('#PasswordUpdate').attr('type',$('#checkboxUpdate').prop('checked')==true?"text":"password"); 
    });
});
</script>
<!-- END -->

  <!-- BEGIN JS::INSERT DATA / STORE -->
    <script>
    $(document).on('click', '.insertchallengeRec', function(){
      var csrfToken  = $('meta[name="csrf-token"]').attr('content');
      var CatType    = $(this).closest("div").find('#CatType').val();
      var CatName    = $(this).closest("div").find('#CatName').val();
      if(CatType == 1){
      CatType = "Postingan";
    }else if(CatType == 2){
      CatType = "Menu FnB";
    }else if(CatType == 3){
      CatType = "Game";
    }
    
      Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data akan masuk ke dalam Database!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: "POST",
            url: "/page_categories/post",
            data:{
              '_method'      : 'POST',
              '_token'       : csrfToken,
              'CatType'      : CatType,
              'CatName'      : CatName,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Inputed!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
    </script>
  <!-- END JS::INSERT DATA / STORE -->

  <script>
    $(document).on('click', '.deletePlayer', function(){
      var csrfToken           = $('meta[name="csrf-token"]').attr('content');
      var deletePlayerId   = $(this).closest("td").find('.deletePlayerId').val();
      alert(deletePlayerId)
      Swal.fire({
        title               : 'Apakah anda yakin ingin menghapus file?',
        text                : 'Data akan di hapus dari Database!',
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#d33',
        cancelButtonColor   : '#3085d6',
        confirmButtonText   : 'Delete!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type                   : "DELETE",
            url                    : "/page_player/delete/{id}",
            data:{
              '_method'            : 'DELETE',
              '_token'             : csrfToken,
              'deletePlayerId'  : deletePlayerId,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
  </script>
</body>

</html>
