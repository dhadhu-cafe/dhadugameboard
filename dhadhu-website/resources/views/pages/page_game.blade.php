<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dashboard Admin Game</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
 
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">

</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Game</h3>
              </div>
              <div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>

            <!-- BEGIN::MODAL TAmbAH DATA -->
            <div id="ModalExample" class="modal fade">
            <!-- begin modal dialog -->
              <div class="modal-dialog">
              <!-- begin modal content -->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Game</h4>
                  </div>
                  <div class ="modal-body">

                    <form role="form" method="POST" action="/page_game/post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Game</label>
                        <input type="text" class="form-control" name="gameName" id="gameName" placeholder="Masukan Nama Game ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Game</label>
                        <select class="browser-default custom-select" name="categoryGame" id="categoryGame">
                          @foreach($categoriesGames as $Cgame)
                          <option value="{{$Cgame->category_name}}">{{$Cgame->category_name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Umur Minimal</label>
                        <input type="text" class="form-control" id="age" name="age" placeholder="Masukan Umur Minimal Pemain ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jumlah Pemain</label>
                        <input type="text" class="form-control" id="jmlplayer" name="jmlplayer" placeholder="Masukan jumlah pemain">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Durasi Permainan</label>
                        <input type="text" class="form-control" name="duration" id="durasi" placeholder="Masukan durasi ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tingkat Kesulitan</label>
                        <select class="browser-default custom-select" name="difficulty" id="difficulty">
                          <option selected="" value="1">Easy</option>
                          <option value="2">Normal</option>
                          <option value="3">Hardcore</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Deskripsi Game</label>
                        <textarea class="form-control" id="descGame" name="descGame" rows="3" placeholder="Masukan Deskripsi..."></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input type="file" class="form-control" id="imageGame" name="imageGame" required>
                      </div>
                      <button type="submit" class="btn btn-primary" id="insertGame">Submit</button>                      
                    </form>
 
                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
            <!-- END::MODAL TAmbAH DATA -->
            
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nama Game</th>
                            <th>Kategori Game</th>
                            <th>Umur Minimal</th>
                            <th>Jumlah Pemain</th>
                            <th>Durasi</th>
                            <th>Tingkat Kesulitan </th>
                            <th>Deskripsi Game </th>
                            <th>Gambar Game</th>
                        </tr> 
                      </thead>
                      <tbody>
                        @foreach($games as $game)
                        <tr>
                          <td align="center">
                            <a type="button" class="btn btn-info" data-toggle="modal" data-target="#modalUpdateGame{{$game->id}}"><em class="fa fa-pencil"></em></a>
                            <button class="btn btn-danger deleteGame deleteGameId" value="{{$game->id}}"><em class="fa fa-trash"></em></button>
                          </td>
                          <td class="hidden-xs">{{$game->id}}</td>
                          <td>{{$game->game_name}}</td>
                          <td>{{$game->category_game}}</td>
                          <td>{{$game->age}}</td>
                          <td>{{$game->jml_player}}</td>
                          <td>{{$game->durasi}}</td>
                          <td>{{$game->difficulty_game}}</td>
                          <td>{{$game->description_game}}</td>
                          <td><img src="{{asset('/gambar/'.$game->image_game) }}"class="img-thumbnail" width="75"></td>
                        </tr>
                        <!-- modal edit data -->
                        <div id="modalUpdateGame{{$game->id}}" tabindex="1"class="modal fade">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4>Edit Data Challenge</h4>
                              </div>
                              <div class ="modal-body">
                                <!-- FORM UPDATE DATA -->
                                <form action="/page_game/edit/{{\Crypt::encryptString($game->id)}}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Game</label>
                                    <input type="text" class="form-control" name="gameName" id="" value="{{$game->game_name}}">
                                  </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Kategori Game</label>
                                      <select class="browser-default custom-select" name="categoryGame" id="">
                                        <option selected="" value="{{$game->category_game}}">{{$game->category_game}}</option>
                                        @foreach($categoriesGames as $Cgame)
                                          <option value="{{$Cgame->category_name}}">{{$Cgame->category_name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Umur Minimal</label>
                                      <input type="text" class="form-control" id="" name="age" value="{{$game->age}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Jumlah Pemain</label>
                                      <input type="text" class="form-control" id="" name="jmlPlayer" value="{{$game->jml_player}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Durasi Permainan</label>
                                      <input type="text" class="form-control" name="durasi" id="" value="{{$game->durasi}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Tingkat Kesulitan</label>
                                      <select class="browser-default custom-select" name="difficultyGame" id="">
                                        <option selected="" value="{{$game->difficulty_game}}">{{$game->difficulty_game}}</option>
                                        <option value="1">Easy</option>
                                        <option value="2">Normal</option>
                                        <option value="3">Hardcore</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Deskripsi Game</label>
                                      <textarea class="form-control" id="" name="descGame" rows="3" >{{$game->description_game}}</textarea>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Gambar</label>
                                      <div>
                                        <input type="hidden" name="prevImage" value="{{$game->image_game}}">
                                        <img src="{{asset('/gambar/'.$game->image_game) }}"class="img-thumbnail" width="75" height="75">
                                      </div>
                                      <input type="file" class="form-control" id="" name="imageGame" required>
                                    </div>
                                  <button type="submit" class="btn btn-primary">Submit</button>     
                                </form>   
                              </div>
                            </div>
                            <!-- modal content end -->
                          </div> 
                          <!-- modal dialog end -->
                        @endforeach  
                        </tbody>
                    </table>
                    {{$games->links()}}
                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->
  @include('sweetalert::alert')
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->

  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
  
  <!-- <script>
  
  $(document).on('click', '.insertDataGame', function(){
    var csrfToken     = $('meta[name="csrf-token"]').attr('content');
    var gameName      = $(this).closest("div").find('#gameName').val();
    var categoryGame  = $(this).closest("div").find('#categoryGame').val();
    // var gendreGame    = $(this).closest("div").find('#gendreGame').val();
    var age           = $(this).closest("div").find('#age').val();
    var jmlplayer     = $(this).closest("div").find('#jmlplayer').val();
    var durasi        = $(this).closest("div").find('#durasi').val();
    var difficulty    = $(this).closest("div").find('#difficulty').val();
    var descGame      = $(this).closest("div").find('#descGame').val();

    if(categoryGame == 1){
      categoryGame = "Romance";
    }else if(categoryGame == 2){
      categoryGame = "Strategy";
    }else if(categoryGame == 3){
      categoryGame = "Action";
    }else if(categoryGame == 4){
      categoryGame = "RPG";
    }

    alert(gameName)
      Swal.fire({
        title               : 'Apakah anda yakin?',
        text                : "Data akan masuk ke dalam Database!",
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        confirmButtonText   : 'Ya!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type              : "POST",
            url               : "/page_game/post",
            data:{
              '_method'       : 'POST',
              '_token'        : csrfToken,
              'gameName'      : gameName,
              'categoryGame'  : categoryGame,              
              'age'           : age,
              'jmlplayer'     : jmlplayer,
              'durasi'        : durasi,
              'difficulty'    : difficulty,
              'descGame'      : descGame,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
  
  </script> -->

<script>
  $(document).on('click', '.deleteGame', function(){
    var csrfToken         = $('meta[name="csrf-token"]').attr('content');
    var deleteGameId      = $(this).closest("td").find('.deleteGameId').val();
    alert(deleteGameId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                : "DELETE",
          url                 : "/page_game/delete/{id}",
          data:{
            '_method'         : 'DELETE',
            '_token'          : csrfToken,
            'deleteGameId'    : deleteGameId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
  </script>
</body>

</html>
