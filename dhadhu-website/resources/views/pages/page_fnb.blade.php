<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard admin F&B</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  
 
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">


</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
    </div>
    <!-- /#sidebar-wrapper -->
  </div>
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>
    <div class="container-fluid">
   	  <br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data F&B</h3>
              </div>
              <div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>

            <div id="ModalExample" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data F&B</h4>
                  </div>
                  <div class ="modal-body">

                    <form role="form" method="POST" action="/page_fnb/post" enctype="multipart/form-data" id="allDataFnb">
                    @csrf
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Makanan / Minuman</label>
                        <input type="text" class="form-control" name="dishName" id="dishName" placeholder="Masukan nama " required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tipe Makanan / Minuman</label>
                        <select class="browser-default custom-select" id="typeDish" name="typeDish" required>
                          @foreach($categoriesFoods as $CFoods)
                          <option value="{{$CFoods->category_name}}">{{$CFoods->category_name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Harga</label>
                        <input type="text" class="form-control" id="priceDish" name="priceDish" placeholder="Masukan Harga " required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi</label>
                        <textarea class="form-control" id="descriptionDish" name="descriptionDish" rows="3" placeholder="Enter ..." required></textarea>
                      </div>
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input type="file" class="form-control" id="imageFnb" name="imageFnb" required>
                      </div>
                      
                      <button type="submit" class="btn btn-primary" id="insertFnb">Submit</button>                      
                    </form>
                    
                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
            <table class="table table-striped table-bordered table-list">
              <thead>
                <tr>
                    <th><em class="fa fa-cog"></em></th>
                    <th class="hidden-xs">ID</th>
                    <th>Nama</th>
                    <th>Tipe</th>
                    <th>Harga</th>
                    <th>Deskripsi</th>
                    <th>Gambar</th>
                </tr> 
              </thead>
              <tbody>
                @foreach($foods as $food)
                  <tr>
                    <td align="center">
                      <a class="btn btn-info" data-toggle="modal" data-target="#modalUpdateFnB{{$food->id}}"><em class="fa fa-pencil"></em></a>
                      <button type="button" class="btn btn-danger deleteMenu deleteMenuId" value="{{$food->id}}"><em class="fa fa-trash"></em></button>
                    </td>
                    <td class="hidden-xs">{{$food->id}}</td>
                    <td>{{$food->name_fnb}}</td>
                    <td>{{$food->type_fnb}}</td>
                    <td>{{$food->price_fnb}}</td>
                    <td>{{$food->description_fnb}}</td>
                    <td><img src="{{asset('/gambar/'.$food->image_fnb) }}"class="img-thumbnail" width="75"></td>
                  </tr>
                    <!-- MODAL EDIT DATA -->
                    <div id="modalUpdateFnB{{$food->id}}" tabindex="1" class="modal fade">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4>Edit Data FnB</h4>
                        </div>
                        <div class ="modal-body">
                          <!-- FORM UPDATE DATA -->
                          <form action="/page_fnb/edit/{{\Crypt::encryptString($food->id)}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama FnB</label>
                              <input type="text" class="form-control" name="nameFnb" id="" value="{{$food->name_fnb}}">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Tipe Makanan / Minuman</label>
                              <select class="browser-default custom-select" id="" name="typeFnb" required>
                                <option selected="" value="{{$food->type_fnb}}">{{$food->type_fnb}}</option>
                                @foreach($categoriesFoods as $CFoods)
                                <option value="{{$CFoods->category_name}}">{{$CFoods->category_name}}</option>
                                @endforeach
                              </select>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Harga</label>
                                <input type="text" class="form-control" id="" name="priceFnb" value="{{$food->price_fnb}}" required>
                              </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <textarea class="form-control" id="" name="descFnb" rows="3" >{{$food->description_fnb}}</textarea>
                              </div>
                              
                              <div class="form-group">
                                <label for="exampleInputEmail1">Gambar</label>
                                  <div>
                                    <input type="hidden" name="prevImage" value="{{$food->image_fnb}}">
                                    <img src="{{asset('/gambar/'.$food->image_fnb) }}" class="img-thumbnail" width="75" height="75">
                                  </div>
                                <input type="file" class="form-control" id="" name="imageFnb" required>
                              </div>

                            <button type="submit" class="btn btn-primary">Submit</button>     
                          </form>   
                        </div>
                        </div>
                      <!-- modal content end -->
                  </div> 
                    <!-- modal dialog end -->
                @endforeach   
                </tbody>
            </table>
            {{$foods->links()}}
          </div>
        </div>
        <!--panel -->
      </div>
      <!--col 12 -->
    </div>
    <!-- /#page-content-wrapper -->
  </div>
</div>
  <!-- /#wrapper -->
  @include('sweetalert::alert')
  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->

  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script>
  $(document).on('click', '.deleteMenu', function(){
    var csrfToken         = $('meta[name="csrf-token"]').attr('content');
    var deleteMenuId      = $(this).closest("td").find('.deleteMenuId').val();
    // alert(deleteMenuId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                : "DELETE",
          url                 : "/page_fnb/delete/{id}",
          data:{
            '_method'         : 'DELETE',
            '_token'          : csrfToken,
            'deleteMenuId'    : deleteMenuId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
  </script>

</body>

</html>
