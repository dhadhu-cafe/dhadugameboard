<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dasboard Admin Score</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> <!-- Custom styles for this template -->
  <link href=" {{asset('assets/css/simple-sidebar.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/css/adminlte.min.css')}}">
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> </div>
      <div class="list-group list-group-flush">
      <a href="/admin/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Dashboard</a>
        <a href="/page_fnb/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data FnB</a>
        <a href="/page_postingan/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Postingan</a>
        <a href="/page_game/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Game</a>
        <a href="/page_score/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Score</a>
        <a href="/page_challenge/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenge</a>
        <a href="/page_challenge_record/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Challenges Record</a>
        <a href="/page_player/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Player</a>
        <a href="/page_room/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Data Room</a>
        <a href="/page_categories/{{\Crypt::encryptString($admin->id)}}" class="list-group-item list-group-item-action bg-light" id="nav-link">Tambah Kategori Baru</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

   <div class="container-fluid">
   	<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Score</h3>
              </div>
              <div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>

            <div id="ModalExample" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Score</h4>
                  </div>
                  <div class ="modal-body">

                    <form role="form" method="POST">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Game</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama game ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail2">Nama Player</label>
                        <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Masukan Nama player ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Score</label>
                        <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Masukan Score ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail4">Record Date Time</label>
                        <input type="text" class="form-control" id="exampleInputEmail4" placeholder="Masukan Record">
                      </div>
                      <button type="button" class="btn btn-primary insertData">Submit</button>                      
                    </form>
                    
                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Id game</th>
                            <th>Id Player</th>
                            <th>Total Score</th>
                            <th>Record Date Time</th>
                        </tr> 
                      </thead>
                      <tbody>
                        @foreach($scores as $score)
                        <tr>
                          <td align="center">
                          <a class="btn btn-info" data-toggle="modal" data-target="#modalUpdateScore{{$score->id}}"><em class="fa fa-pencil"></em></a>
                            <button class="btn btn-danger deleteScore deleteScoreId" value="{{$score->id}}"><em class="fa fa-trash"></em></button>
                          </td>
                          <td class="hidden-xs">{{$score->id}}</td>
                          <td>{{$score->game_name}}</td>
                          <td>{{$score->player_name}}</td>
                          <td>{{$score->score}}</td>
                          <td>{{$score->playtime}}</td>
                        </tr>
                        <!-- MODAL EDIT DATA -->
                     <div id="modalUpdateScore{{$score->id}}" tabindex="1"class="modal fade">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4>Edit Data Score</h4>
                          </div>
                          <div class ="modal-body">
                          <!-- FORM UPDATE DATA -->
                            <form action="/page_score/edit/{{\Crypt::encryptString($score->id)}}" method="POST">
                              @csrf
                              <div class="form-group">
                                <label for="exampleInputEmail1">Game Name</label>
                                <input type="text" class="form-control" name="CGameIdUpdate" id="CGameIdUpdate" value="{{$score->game_name}}">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail2">Player Name</label>
                                <input type="text" class="form-control" name="CPlayerIdUpdate"id="CPlayerIdUpdate" value="{{$score->player_name}}">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3"> Score</label>
                                <input type="text" class="form-control" name="CScoreUpdate"id="CScoreUpdate" value="{{$score->score}}">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail4">Record Date Time</label>
                                <input type="text" class="form-control" name="CPlaytimeUpdate" id="CPlaytimeUpdate" value="{{$score->playtime}}">
                              </div>
                              <button type="submit" class="btn btn-primary">Submit</button>     
                            </form>   
                          </div>
                        </div>
                        <!-- modal content end -->
                      </div> 
                      <!-- modal dialog end -->
                        @endforeach
                      </tbody>
                    </table>
                {{$scores -> links()}}
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>
    <!-- /#page-content-wrapper -->

</div>
</div>
  <!-- /#wrapper -->

  <!-- begin::alert -->
  @include('sweetalert::alert')
  <!-- End::alert -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/js/adminlte.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Menu Toggle Script -->

  <!-- SWEET ALERT -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- SWEET ALERT -->
  
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <!-- BEGIN JS::INSERT DATA / STORE -->
    <script>
    $(document).on('click', '.insertData', function(){
      var csrfToken     = $('meta[name="csrf-token"]').attr('content');
      var gameId        = $(this).closest("div").find('#exampleInputEmail1').val();
      var playerId      = $(this).closest("div").find('#exampleInputEmail2').val();
      var score    = $(this).closest("div").find('#exampleInputEmail3').val();
      var playtime      = $(this).closest("div").find('#exampleInputEmail4').val();
      // alert(gameId, playerId, totalScore, playtime)
      Swal.fire({
        title               : 'Apakah anda yakin?',
        text                : "Data akan masuk ke dalam Database!",
        icon                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        confirmButtonText   : 'Ya!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type            : "POST",
            url             : "/page_score/post",
            data:{
              '_method'     : 'POST',
              '_token'      : csrfToken,
              'game_name'   : gameId,
              'player_name' : playerId,
              'score'       : score,
              'playtime'    : playtime,
            },
            success: function(data){
              console.log(data)
              if(data.result == 'success'){
                Swal.fire(
                  'Success!',
                  data['message'],
                  'success'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }else{
                Swal.fire(
                  'Error!',
                  data['message'],
                  'error'
                )
                // BEGIN::Reload page
                .then((result)=>{
                  location.reload();
                })
                // END::Reload page
              }
            }
            
          })
        }
      })
    })
    </script>
  <!-- END JS::INSERT DATA / STORE -->

  <script>
  $(document).on('click', '.deleteScore', function(){
    var csrfToken         = $('meta[name="csrf-token"]').attr('content');
    var deleteScoreId      = $(this).closest("td").find('.deleteScoreId').val();
    alert(deleteScoreId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                : "DELETE",
          url                 : "/page_score/delete/{id}",
          data:{
            '_method'         : 'DELETE',
            '_token'          : csrfToken,
            'deleteScoreId'    : deleteScoreId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
  </script>
</body>

</html>
