<!-- START::MODAL GAME -->
@foreach($topGames as $topGame)
<div class="modal-container" id="gameModal-{{$topGame->id}}">
  <div class="modal-body">

    <div class="modal-container-name">
      <p>{{$topGame->game_name}}</p>
    </div>

    <div class="modal-container-value">
      <img src="{{asset('gambar/'.$topGame->image_game)}}" class="modal-custome-image">
    </div>

    <div class="modal-container-details">
      <div class="modal-spec">
        <h5>Category Game : {{$topGame->category_game}} | Usia : {{ $topGame->age}}</h5>
        <h5> Max {{$topGame->jml_player}} Player | Difficulty {{$topGame->difficulty_game}}</h5>
      </div>

      <div class="modal-spec-how-to-play">
        <h5>some link ???</h5>
      </div>

      <div class="modal-spec-descrip">
        {{ $topGame->description_game }}
      </div>

      <hr>

      <div class="modal-container-info">
        <p>Dhadhu Board Game Cafe</p>
      </div>
    </div>

  </div>
</div> 
@endforeach
<!-- END::MODAL TOP GAME -->

