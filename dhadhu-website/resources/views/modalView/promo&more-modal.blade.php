@foreach($events as $event)
<div class="modal-container" id="pnmModal-{{$event->id}}">
  <div class="modal-body">

    <div class="modal-container-name">
      <p>{{$event->event_name}}</p>
      <p>{{$event->category}}</p>
    </div>

    <div class="modal-container-value">
      <img src="{{asset('gambar/'.$event->image_event)}}" class="modal-custome-image">
    </div>

    <div class="modal-container-details">
      <h5>{{$event->category}}</h5>

      <hr>
      
      <div class="modal-container-info">
        <p>Dhadhu Board Game Cafe</p>
      </div>
    </div>

  </div>
</div> 
@endforeach