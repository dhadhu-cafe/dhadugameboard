@foreach($menus as $menu)
<div class="modal-container" id="fnbModal-{{$menu->id}}">
  <div class="modal-body">

    <div class="modal-container-name">
      <p>{{$menu->name_fnb}}</p>
      <p>{{$menu->type_fnb}}</p>

    </div>

    <div class="modal-container-value">
      <img src="{{asset('gambar/'.$menu->image_fnb) }}" class="modal-custome-image">
    </div>

    <div class="modal-container-details">
      <h5>{{$menu->description_fnb}}</h5>
        <div class="modal-container-price-box">
          <p>{{$menu->price_fnb}}</p>
        </div>

        <hr>
        
        <div class="modal-container-info">
          <p>Dhadhu Board Game Cafe</p>
        </div>
    </div>

  </div>
</div> 
@endforeach