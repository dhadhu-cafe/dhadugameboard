<!-- START::MODAL GAME -->
@foreach($games as $game)
<div class="modal-container" id="gxModal-{{$game->id}}">
  <div class="modal-body">

    <div class="modal-container-name">
      <p>{{$game->game_name}}</p>
    </div>

    <div class="modal-container-value">
      <img src="{{asset('gambar/'.$game->image_game)}}" class="modal-custome-image">
    </div>

    <div class="modal-container-details">
      <div class="modal-spec">
        <h5>Category Game : {{$game->category_game}} | Usia Minimal : {{ $game->age}}</h5>
        <h5>Max {{$game->jml_player}} Player | Difficulty {{$game->difficulty_game}}</h5>
      </div>  

      <div class="modal-spec-how-to-play">
        <h5>some link ???</h5>
      </div>

      <div class="modal-spec-descrip">
        {{ $game->description_game }}
      </div>

      <hr>

      <div class="modal-container-info">
        <p>Dhadhu Board Game Cafe</p>
      </div>
    </div>
  
  </div>
</div> 
@endforeach
<!-- END::MODAL GAME -->