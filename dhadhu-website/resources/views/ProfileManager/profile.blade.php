<!DOCTYPE html>
<html lang="en">
<head>
    @include('head')
</head>
<body>
    <div class="main-container-profile">
        <div class="container-profile">
            <div class="header-container-profile">
                <p>Profile Manager</p>
            </div>

            <div class="first-layer-profile">
                
                <div class="image-profile">
                    @if($dataPlayer->player_image != null)
                    <img src="{{asset('/gambar/'.$dataPlayer->player_image) }}" alt="PP" class="image-profile-custome">
                    @else
                    <img src="{{asset('assets/foto/img_avatar2.png')}}" alt="PP" class="image-profile-custome">
                    @endif
                </div>
                                
                <div class="nickname-profile">
                    <p>{{$dataPlayer->player_name}}</p>
                    <p>{{$dataPlayer->exp}}</p>
                </div>
                
            </div>

            <div class="header-container-profile">
                <p>Profile Control</p>
            </div>

            <div class="second-layer-profile">

                <form action="/profilemanager/{{\Crypt::encryptString($dataPlayer->id)}}/edit" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="edit-profile active">
                        <label for="">Nick Name</label> <br> 
                        <input type="text" name="player_name" id="" placeholder="Get Nick Name" value="{{$dataPlayer->player_name}}" required> <br>

                        <div class="split-profile">
                            <div class="content-split">
                                <label for="">First Name</label> <br>
                                <input type="text" name="fname" id="" placeholder="Get First Name" value="{{$dataPlayer->fname}}" required> 
                            </div>

                            <div class="content-split">
                                <label for="">Last Name</label> <br>
                                <input type="text" name="lname" id="" placeholder="Get Last Name" value="{{$dataPlayer->lname}}" required> 
                            </div>
                        </div>
                        
                        <label for="">Email</label> <br>
                        <input type="email" name="email" id="" placeholder="Get Email" value="{{$dataPlayer->email}}" Readonly> <br>
                        
                        <div class="split-profile">
                            <div class="content-split">
                                <label for="">Password</label> <br>
                                <input type="password" name="password" id="" placeholder="Get Password" value="{{$dataPlayer->password}}" required> <br>
                            </div>

                            <div class="content-split">
                                <label for="">Confirm Password</label> <br>
                                <input type="password" name="cfmPassword" id="" placeholder="Get Confirm Password" value="{{$dataPlayer->cfmPassword}}" required> <br>
                            </div>
                        </div>
                        
                        <label for="">Profile Picture</label> <br>
                        @if($dataPlayer->player_image != null)

                            <img src="{{asset('/gambar/'.$dataPlayer->player_image) }}"class="img-thumbnail">
                            <img src="{{asset('/gambar/'.$dataPlayer->player_image) }}"class="img-thumbnail medium">
                            <img src="{{asset('/gambar/'.$dataPlayer->player_image) }}"class="img-thumbnail small">
                            <input type="hidden" src="" alt="" class="prevImage" name="prevImage" value="{{$dataPlayer->player_image}}"> <br>
                            @else
                            @endif
                            <input type="file" src="" alt="" class="tCoba" name="profileImage" value="{{$dataPlayer->player_image}}"> <br>
             
                        
                        <button type="submit">Edit</button>
                        <button><a href="/dashboard/{{\Crypt::encryptString($user->id)}}">Back to Menu</a></button>
                    </div>

                </form>
            </div>

            <div class="header-container-profile">
                <p>History</p>
            </div>
            @if($historys != null)
            <div class="fourth-layer-profile">
            
                <div class="history">
                
                    <div class="split-profile">
                        
                        <div class="content-split">
                            <div class="recent-nickname">
                                <p>nickname</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>game name</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>high score</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>total score</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>time trial</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-date">
                                <p>date</p>
                            </div>
                        </div>
                    
                    </div>

                    @foreach($historys as $history)
                    <div class="split-profile">
                        
                        <div class="content-split">
                            <div class="recent-nickname">
                                <p>{{$history->player_name}}</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>{{$history->game_name}}</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>{{$history->score}}</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>{{$history->total_score}}</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>{{$history->playtime}}</p>
                            </div>
                        </div>

                        <div class="content-split">
                            <div class="recent-date">
                                <p>{{$history->created_at}}</p>
                            </div>
                        </div>
                    
                    </div>
                    @endforeach
                </div>   
            </div>
            @else
            <div class="fourth-layer-profile">
                <div class="history">
                    <div class="split-profile">
                        <div class="content-split">
                            <div class="recent-nickname">
                                <p>nickname</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>room name</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>game name</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>high score</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>total score</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-room-name">
                                <p>time trial</p>
                            </div>
                        </div>
                        <div class="content-split">
                            <div class="recent-date">
                                <p>date</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    
    <!-- SWEET ALERT -->
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        @include('sweetalert::alert')
    <!-- SWEET ALERT -->

</body>
</html>