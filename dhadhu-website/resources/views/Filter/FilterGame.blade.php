<div class="container-filter" id="filterGame">
    <div class="filter-body">
        <div class="inside-filter-body">
        <form action="">
            <div class="filter-search">
                <h3>Filter Option</h3>
                <datalist id="suggestionsEvent">
                    @foreach($games as $game)
                    <option value="{{$game->game_name}}"></option>
                    @endforeach
                </datalist>
                <input placeholder="search.." id="searchGames" name="searchGames" class="searchGames" autoComplete="on" list="suggestionsEvent"/> 
            </div>
            <div class="filter-category">
                <h3>Category</h3>

                <div class="div-category-option">
                    <input type="radio" id="Mudah" name="selectRowDifficulty" value="Easy">
                    <label for="Mudah">Easy</label><br>
                </div>
                <div class="div-category-option">
                    <input type="radio" id="Normal" name="selectRowDifficulty" value="Normal">
                    <label for="Normal">Normal</label><br>
                </div>
                <div class="div-category-option">
                    <input type="radio" id="Hardcore" name="selectRowDifficulty" value="Hardcore">
                    <label for="Hardcore">Hardcore</label><br>
                </div>

            </div>

            <div class="filter-category">
                <h3>Max Player</h3>
                <!-- <div class="div-category-option">
                    <input type="radio" id="1" name="selectRowMaxp" value="1">
                    <label for="1">1</label><br>
                </div> -->

                <div class="div-category-option">
                    <input type="radio" id="2" name="selectRowMaxp" value="2">
                    <label for="2">2</label><br>
                </div>

                <div class="div-category-option">                    
                    <input type="radio" id="3" name="selectRowMaxp" value="3">
                    <label for="3">3</label><br>
                </div>

                <div class="div-category-option">
                    <input type="radio" id="4" name="selectRowMaxp" value="4">
                    <label for="4">4</label><br>
                </div>

                <div class="div-category-option">                   
                    <input type="radio" id="4+" name="selectRowMaxp" value="more">
                    <label for="4+">Lebih Dari 4</label><br>
                </div>
            </div>

            <div class="filter-category">
                <h3>Durasi</h3>
                <div class="div-category-option">
                    <input type="radio" id="Sebentar" name="selectRowDur" value="30">
                    <label for="Sebentar">Sebentar</label><br>
                </div>

                <div class="div-category-option">
                    <input type="radio" id="Lama" name="selectRowDur" value="31">
                    <label for="Lama">Lama</label><br>
                </div>
            </div>

            <div class="filter-category">
                <h3>Usia</h3>
                <!-- <div class="div-category-option">
                    <input type="radio" id="BO" name="selectRowAge" value="12">
                    <label for="BO">Anak kecil</label><br>
                </div> -->

                <div class="div-category-option">
                    <input type="radio" id="Teen" name="selectRowAge" value="17">
                    <label for="Teen">Teen</label><br>
                </div>

                <div class="div-category-option">
                    <input type="radio" id="Adult" name="selectRowAge" value="18">
                    <label for="Adult">Adult</label><br>
                </div>
                <div class="div-category-option">
                    <input type="radio" id="SU" name="selectRowAge" value="0">
                    <label for="SU">All age</label><br>
                </div>
            </div>
            <button type="button" onclick="multipleFiltersGame()">OK</button>
        </form>
            

            
        </div>
    </div>
</div>