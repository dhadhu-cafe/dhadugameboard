<div class="container-filter" id="filterPromo">
    <div class="filter-body">
        <div class="inside-filter-body">
        <form action="">
            <div class="filter-search">
                <h3>Filter Option</h3>
                <datalist id="suggestionsEvent">
                    @foreach($events as $event)
                    <option value="{{$event->event_name}}"></option>
                    @endforeach
                </datalist>
                <input placeholder="search.." id="searchEvents" name="searchEvents" class="searchEvents" autoComplete="on" list="suggestionsEvent"/> 
            </div>
            
            <div class="filter-date" id="month">
                <h3>Date</h3>
                <div class="div-date-option" id="month-1">
                    <input type="radio" id="January" name="selectRow[]" value="1">
                    <label for="January">January</label><br>
                </div>

                <div class="div-date-option" id="month-2">
                    <input type="radio" id="February" name="selectRow[]" value="2">
                    <label for="February">February</label><br>
                </div>

                <div class="div-date-option" id="month-3">
                    <input type="radio" id="March" name="selectRow[]" value="3">
                    <label for="March">March</label><br>
                </div>

                <div class="div-date-option" id="month-4">
                    <input type="radio" id="April" name="selectRow[]" value="4">
                    <label for="April">April</label><br>
                </div>

                <div class="div-date-option" id="month-5">
                    <input type="radio" id="May" name="selectRow[]" value="5">
                    <label for="May">May</label><br>
                </div>

                <div class="div-date-option" id="month-6">
                    <input type="radio" id="June" name="selectRow[]" value="6">
                    <label for="June">June</label><br>
                </div>

                <div class="div-date-option" id="month-7">
                    <input type="radio" id="July" name="selectRow[]" value="7">
                    <label for="July">July</label><br>
                </div>

                <div class="div-date-option" id="month-8">
                    <input type="radio" id="August" name="selectRow[]" value="8">
                    <label for="August">August</label><br>
                </div>

                <div class="div-date-option" id="month-9">
                    <input type="radio" id="September" name="selectRow[]" value="9">
                    <label for="September">September</label><br>
                </div>

                <div class="div-date-option" id="month-10">
                    <input type="radio" id="October" name="selectRow[]" value="10">
                    <label for="October">October</label><br>
                </div>

                <div class="div-date-option" id="month-11">
                    <input type="radio" id="November" name="selectRow[]" value="11">
                    <label for="November">November</label><br>
                </div>

                <div class="div-date-option" id="month-12">
                    <input type="radio" id="December" name="selectRow[]" value="12">
                    <label for="December">December</label><br>
                </div>
            </div>

            <div class="filter-category">
                <h3>Category</h3>
                @foreach($events as $event)
                <div class="div-category-option">
                    <input type="radio" id="category" name="selectRowEvent[]" value="{{$event->category}}">
                    <label for="category">{{$event->category}}</label><br>
                </div>
                @endforeach
            </div>
            <button type="button" onclick="multipleFilters()">OK</button>
            </form>
            
        </div>
        <!-- action button -->
    </div>
</div>