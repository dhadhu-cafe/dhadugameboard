<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Dhadhu Game Board Cafe</title>

<!-- Normal Style -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" type="text/css" crossorigin="">
<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
<link href="{{asset('assets/css/style-no1-main-style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/style-no2-pagination.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/style-no3-container.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/style-no6-modal.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/style-no7-filter.css')}}" rel="stylesheet" type="text/css">
<!-- Normal Style -->

<!-- Mobile Style -->
<link href="{{asset('assets/css/responsive/mobile.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="{{asset('assets/css/responsive/laptop.css')}}" rel="stylesheet" type="text/css"> -->
<!-- <link href="{{asset('assets/css/responsive/desktop.css')}}" rel="stylesheet" type="text/css"> -->
<!-- Mobile Style -->

<!-- BEGIN : Bootstrap CSS -->
<script src="https://kit.fontawesome.com/6430f95176.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- END : Bootstrap CSS -->
