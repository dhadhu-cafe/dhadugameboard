<div class="eventx">
    <h2>Events</h2>
    <div class="sub-eventx">
        @if(count($eventsDashboard))
        @foreach($eventsDashboard as $event)
        <div class="container-event">
            <div class="pas-foto"  onclick="show({{$event->id}})">
                <img src="{{asset('gambar/'.$event->image_event)}}" alt="PasFoto">
            </div>
            <div class="container-nama-harga">
                <h5>{{$event->event_name}}</h5>
                <p>{{$event->category}}</p>
            </div>  
        </div>
        @endforeach
        @else
        <div class="container-not-found">
            <p>No data found!</p>
        </div>
        @endif    
    </div>
</div>

<!-- END:: EVENTs IN DASHBOARD -->

<div class="eventx">
    <h2>Weekly Plays</h2>
    <div class="sub-eventx">
        @if($topGames!=null)
        @foreach($topGames as $topGame)
        <div class="container-event">
            <div class="pas-foto">
                <img src="{{asset('gambar/'.$topGame->image_game) }}" alt="PasFoto">
            </div>
            <div class="container-nama-harga">
                <h5>{{$topGame->game_name}}</h5>
                <p>{{$topGame->difficulty_game}}</p>
            </div>  
        </div>
        @endforeach
        @else
        <div class="container-not-found">
            <p>No data found!</p>
        </div>
        @endif
    </div>
</div>
<!-- modal -->
@include('modalView.dashboard-modal')
<!-- modal --> 

<div class="eventx">
<h2>Your Game</h2>
    <div class="container-event">
        @if($recentPlay!=null)
        <div class="pas-foto">
            <img src="{{asset('gambar/'.$recentPlay->image_game)}}" alt="PasFoto">
        </div>
        <div class="container-nama-harga">
        <h5>{{$recentPlay->game_name}}</h5>
        <p>{{$recentPlay->difficulty_game}}</p>
        </div>
        @else
        <div></div>
        @endif
    </div>

    <div class="container-event">
        @if($favGame!=null)
        <div class="pas-foto">
            <img src="{{asset('gambar/'.$favGame->image_game)}}" alt="PasFoto">
        </div>
        <div class="container-nama-harga">
        <h5>{{$favGame->game_name}}</h5>
        <p>{{$favGame->difficulty_game}}</p>
        </div>
        @else
        <div></div>
        @endif
    </div>
</div>
<!-- modal -->
@include('modalView.dashboard-modal')
<!-- modal -->