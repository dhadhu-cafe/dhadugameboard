<h1 id="title" style="padding: 10px">Favorite Restaurants</h1>
<div id='map'></div>

    <div class="container-info">
        <div class="info-dhadhu right">
            <h2>Kontak</h2>
            @foreach($info as $info)
            <h4>{{$info->kontak}}</h4>
            
        </div>
        
        <div class="info-dhadhu center">
            <h2>Alamat</h2>
            <h4>{{$info->Alamat}}</h4>
        </div>

        <div class="info-dhadhu left">
            <h2>Jam Buka</h2>
            <table id="profile-table">
            <tr>
                <td>Sen</td>
                <td>:</td>
                <td>{{$info->Senin}}</td>
            </tr>
            <tr>
                <td>Sel</td>
                <td>:</td>
                <td>{{$info->Selasa}}</td>
            </tr>
            <tr>
                <td>Rab</td>
                <td>:</td>
                <td>{{$info->Rabu}}</td>
            </tr>
            <tr>
                <td>Kam</td>
                <td>:</td>
                <td>{{$info->Kamis}}</td>
            </tr>
            <tr>
                <td>Jum</td>
                <td>:</td>
                <td>{{$info->Jumat}}</td>
            </tr>
            <tr>
                <td>Sab</td>
                <td>:</td>
                <td>{{$info->Sabtu}}</td>
            </tr>
            <tr>
                <td>Min</td>
                <td>:</td>
                <td>{{$info->Sabtu}}</td>
            </tr>
            </table>
            @endforeach
    </div>
</div>