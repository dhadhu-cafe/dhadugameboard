<div class="play-tab" id="roomControl">
    <div class="room-control">
        <table id="table-room-control">
            <tr>
                <th>No.</th>
                <th>Nick Name Player</th>
                <th>Level-Point</th>
                <th>Status</th>
            </tr>
            @if($roomMaster == true)
            
            <tbody class="roomMasterCek">
            <?php $no = 0;?>
            @foreach($detailRoomAsMasters as $detailRoomAsMaster)
            <?php $no++;?>
            <tr>
                <td>{{$no}}</td>
                <td>{{$detailRoomAsMaster->player_name}}</td>
                <td>{{$detailRoomAsMaster->level_player}}</td>
                @if($detailRoomAsMaster->status == 'joined')
                <td>In Room</td>
                @else
                <td>Waiting</td>
                @endif
            </tr>
            @endforeach
            </tbody>
            @elseif($joinRoom == true)
            
            <?php $no = 0;?>
            @foreach($dataMembers as $dataMember)
            <?php $no++;?>
            <tr>
                <td>{{$no}}</td>
                <td>{{$dataMember->player_name}}</td>
                <td>{{$dataMember->level_player}}</td>
                @if($dataMember->status == 'joined')
                <td>In Room</td>
                @else
                <td>Waiting</td>
                @endif
            </tr>
            @endforeach

            @endif
        </table>
    </div>


    <div class="room-control right roomDetailCek">
        @if($roomMaster == true)
        <div class="room-section-edit">
            <div class="title-room-control">
                <p>Edit Room</p>
            </div>
            
            <div class="split-room-section">
                <div class="content-split-room">
                    <label for="">Room Name</label> <br>
                    <input type="text" name="" id="" placeholder="Room Name" value="{{$controllRoom->room_name}}"> <br>
                    <input class="hidden display: none" type="hidden" name="" id="" placeholder="Room Name" value="{{$controllRoom->id}}">
                </div>
                
                <div class="content-split-room">
                    <label for="">Game Name</label><br>
                    <input type="text" name="" id="" placeholder="Game Name" value="{{$controllRoom->game_name}}"> <br>
                </div>
            </div>
            
            <div class="split-room-section">
                <div class="content-split-room">
                    <label for="">Max Player</label> <br>
                    <input type="number" name="" id="" placeholder="Max Player" value="{{$controllRoom->max_players}}"> <br>
                </div>

                <div class="content-split-room">
                    <label for="">Date Play</label> <br>
                    <input type="date" name="" id="" value="{{$controllRoom->date}}"> <br>
                </div>
            </div>
           
            <label for="">Description</label><br>
            <input type="text" name="" id="" placeholder="Description" value="{{$controllRoom->description_room}}"> <br>
            
            <div class="play-option">
                <button>Edit</button> &nbsp
                <form method="POST" action="/dashboard/destroy-room/{{$controllRoom->id}}">
                    @method('DELETE')
                    @csrf
                    <button type="submit">Bubarkan Room</button>
                </form>
            </div>

        </div>
        @elseif($joinRoom == true)
        <div class="room-section-edit">
            <div class="title-room-control">
                <p>Edit Room</p>
            </div>
            
            <div class="split-room-section">
                <div class="content-split-room">
                    <label for="">Room Name</label> <br>
                    <input type="text" name="" id="" placeholder="Room Name" value="{{$controllRoomForMember->room_name}}" readonly> <br>
                    <input class="hidden display: none" type="hidden" name="" id="" placeholder="Room Name" value="{{$controllRoomForMember->id}}" readonly>
                </div>

                <div class="content-split-room">
                    <label for="">Game Name</label><br>
                    <input type="text" name="" id="" placeholder="Game Name" value="{{$controllRoomForMember->game_name}}" readonly> <br>
                </div>
            </div>
            
            <div class="split-room-section">
                <div class="content-split-room">
                    <label for="">Max Player</label> <br>
                    <input type="text" name="" id="" placeholder="Max Player" value="{{$controllRoomForMember->max_players}}" readonly> <br>
                </div>

                <div class="content-split-room">
                    <label for="">Date Play</label> <br>
                    <input type="date" name="" id="" value="{{$controllRoomForMember->date}}" readonly> <br>
                </div>
            </div>
           
            <label for="">Description</label><br>
            <input type="text" name="" id="" placeholder="Description" value="{{$controllRoomForMember->description_room}}" readonly> <br>
            
            <div class="play-option">
                <form method="POST" action="/dashboard/leave-room/{{$controllRoomForMember->t_players_id}}">
                <input type="hidden" name="roomId" value="{{$controllRoomForMember->id}}">
                @method('DELETE')
                @csrf
                <button type="submit">Tinggalkan Room</button>
                </form>
            </div>
        </div>
        @endif
    </div>

    
</div>

<div class="play-tab active">
    <h2>All Joined Room</h2>
<!-- Start -->
<div class="container-quest">
        <div class="container-name-quest">
        <table id="play-table">
            <tr>
                <th>No Room</th>
                <th>Game</th>
                <th>Room Name</th>
                <th>Room Description</th>
                <th>Max Player</th>
                <th>action</th>
            </tr>

            <?php $no = 0;?>
            @foreach($joinedRooms as $joinedRoom)
            <?php $no++;?>
            <form method="POST" action="/dashboard/{{$user->id}}/joinRoom">
            @csrf
            <tr onclick="cekRoom({{$joinedRoom->id}}, {{$joinedRoom->t_players_id}})">
                <td>{{$no}}</td>
                <input type="hidden" name="roomId" value="{{$joinedRoom->id}}">
                <td>{{$joinedRoom->game_name}}</td>
                <input type="hidden" name="gameId" value="{{$joinedRoom->t_games_id}}">
                <td>{{$joinedRoom->room_name}}</td>
                <input type="hidden" name="roomName" value="{{$joinedRoom->room_name}}">
                <td>{{$joinedRoom->description_room}}</td>
                <input type="hidden" name="dscRoom" value="{{$joinedRoom->description_room}}">
                <td>{{$joinedRoom->max_players}}</td>
                <input type="hidden" name="maxPlayers" value="{{$joinedRoom->max_players}}">
                <input type="hidden" name="maxPlayers" id="player-{{$joinedRoom->t_players_id}}" value="{{$joinedRoom->t_players_id}}">
                <td> <button type="button">cek</button>
                </td>
            </tr>
            </form>
            @endforeach
        </table>
        </div>
    </div>
    <hr>

<!-- End -->

    <h2>Most Anticipated Plays</h2>
    <!-- Start -->
    <div class="container-quest">
        <div class="container-name-quest">
        <table id="play-table">
            <tr>
                <th>No Room</th>
                <th>Game</th>
                <th>Room Name</th>
                <th>Room Description</th>
                <th>Max Player</th>
                <th>action</th>
            </tr>

            <?php $no = 0;?>
            @foreach($newRooms as $newRoom)
            <?php $no++;?>
            <form method="POST" action="/dashboard/{{$user->id}}/joinRoom">
            @csrf
            <tr>
                <td>{{$no}}</td>
                <input type="hidden" name="roomId" value="{{$newRoom->id}}">
                <td>{{$newRoom->game_name}}</td>
                <input type="hidden" name="gameId" value="{{$newRoom->t_games_id}}">
                <td>{{$newRoom->room_name}}</td>
                <input type="hidden" name="roomName" value="{{$newRoom->room_name}}">
                <td>{{$newRoom->description_room}}</td>
                <input type="hidden" name="dscRoom" value="{{$newRoom->description_room}}">
                <td>{{$newRoom->max_players}}</td>
                <input type="hidden" name="maxPlayers" value="{{$newRoom->max_players}}">
                <td> <button type="submit">Join</button> 
                </td>
            </tr>
            </form>
            @endforeach
        </table>
        </div>
    </div>
    
    <!-- End -->
    <hr>

    <h2>All Upcoming Game</h2>
    <!-- Start -->

    <div class="container-quest">
        <div class="container-name-quest">
        <table id="play-table">
            <tr>
                <th>No Room</th>
                <th>Game</th>
                <th>Room Name</th>
                <th>Room Description</th>
                <th>Max Player</th>
                <th>action</th>
            </tr>
            <?php $no = 0;?>
            @foreach($newRooms as $newRoom)
            <?php $no++;?>
            <form method="POST" action="/dashboard/{{$user->id}}/joinRoom">
            @csrf
            <tr>
                <td>{{$no}}</td>
                <input type="hidden" name="roomId" value="{{$newRoom->id}}">
                <td>{{$newRoom->game_name}}</td>
                <input type="hidden" name="gameId" value="{{$newRoom->t_games_id}}">
                <td>{{$newRoom->room_name}}</td>
                <input type="hidden" name="roomName" value="{{$newRoom->room_name}}">
                <td>{{$newRoom->description_room}}</td>
                <input type="hidden" name="dscRoom" value="{{$newRoom->description_room}}">
                <td>{{$newRoom->max_players}}</td>
                <input type="hidden" name="maxPlayers" value="{{$newRoom->max_players}}">
                <td> <button type="submit">Join</button> 
                </td>
            </tr>
            </form>
            @endforeach
        </table>
        </div>
    </div>
    
    <!-- End -->
    <hr>
</div>

<form method="POST" action="/dashboard/{{$user->id}}/createRoom">
@csrf
<div class="play-tab">
    <h3 style="text-align: center">Create Room</h3>
    <div class="container-room">
        <div class="play-room">
            <label for="">Room Name</label><br>
            <input type="text" name="roomName" id="roomName" placeholder="Room Name" required><br>

            <label for="">Game Name</label><br>
            <div>
                <datalist id="suggestions">
                @foreach($games as $game)
                <option value="{{$game->game_name}}">{{$game->game_name}}</option>
                @endforeach
                </datalist>
                <input placeholder="search.." name="gameName" id="gameName" autoComplete="on" list="suggestions" required /> 
            </div>

            <label for="">Description</label><br>
            <input type="text" name="descriptionRoomGame" id="descriptionRoomGame" placeholder="Description" required><br>
        </div>

        <div class="play-room">
            <label for="">Max Player</label>  <br>
            <input type="number" name="maxPLayer" id="maxPLayer" placeholder="Max Player" required> <br>
            
            <label for="">Date Play</label><br>
            <input type="date" name="date" id="date" required><br>
            
            <button type="submit" id="btnCreate">Create Room</button>
            <!-- alert success -->
                @if(session()->has('successCrateRoom'))
                <div class="alert alert-success" style="color: green;font-weight:bold">
                    {{ session()->get('successCrateRoom') }}
                </div>
                @endif
            <!-- END::Alert success -->

        </div>
    </div>
</div>
</form>




