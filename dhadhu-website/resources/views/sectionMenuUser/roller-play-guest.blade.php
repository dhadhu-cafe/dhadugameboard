<div class="container-challenge">
    <h2>Playground</h2>
    <div class="container-challenge-navbar">
        <div class="challenge-navbar-menu" id="DivPlay">
            <div class="nav-play turnOn">All Upcoming Plays</div> 
        </div>
    </div>

    <div class="play-tab active">
    <h2>Most Anticipated Plays</h2>
    <!-- Start -->
    <div class="container-quest">
        <div class="container-name-quest">
        <table id="play-table">
            <tr>
                <th>No Room</th>
                <th>Game</th>
                <th>Room Name</th>
                <th>Room Description</th>
                <th>Max Player</th>
            </tr>

            <?php $no = 0;?>
            @foreach($newRooms as $newRoom)
            <?php $no++;?>
            @csrf
            <tr>
                <td>{{$no}}</td>
                <input type="hidden" name="roomId" value="{{$newRoom->id}}">
                <td>{{$newRoom->game_name}}</td>
                <input type="hidden" name="gameId" value="{{$newRoom->t_games_id}}">
                <td>{{$newRoom->room_name}}</td>
                <input type="hidden" name="roomName" value="{{$newRoom->room_name}}">
                <td>{{$newRoom->description_room}}</td>
                <input type="hidden" name="dscRoom" value="{{$newRoom->description_room}}">
                <td>{{$newRoom->max_players}}</td>

            </tr>
            @endforeach
        </table>
        </div>
    </div>
    
    <!-- End -->

    <h2>All Upcoming Game</h2>
    <!-- Start -->

    <div class="container-quest">
        <div class="container-name-quest">
        <table id="play-table">
            <tr>
                <th>No Room</th>
                <th>Game</th>
                <th>Room Name</th>
                <th>Room Description</th>
                <th>Max Player</th>
            </tr>
            <?php $no = 0;?>
            @foreach($newRooms as $newRoom)
            <?php $no++;?>
            @csrf
            <tr>
                <td>{{$no}}</td>
                <input type="hidden" name="roomId" value="{{$newRoom->id}}">
                <td>{{$newRoom->game_name}}</td>
                <input type="hidden" name="gameId" value="{{$newRoom->t_games_id}}">
                <td>{{$newRoom->room_name}}</td>
                <input type="hidden" name="roomName" value="{{$newRoom->room_name}}">
                <td>{{$newRoom->description_room}}</td>
                <input type="hidden" name="dscRoom" value="{{$newRoom->description_room}}">
                <td>{{$newRoom->max_players}}</td>
                <input type="hidden" name="maxPlayers" value="{{$newRoom->max_players}}">
            </tr>
            @endforeach
        </table>
        </div>
    </div>
    
    <!-- End -->
</div>