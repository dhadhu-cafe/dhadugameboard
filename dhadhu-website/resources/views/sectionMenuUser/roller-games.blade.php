<!-------------------------- BEGIN::TOP GAMES -------------------------->
<div class="event-game">
    <h2>Top Games</h2>
    @if($topGames!=null)
    @foreach($topGames as $topGame)
    <div class="container-event-game">
        <div class="pas-foto">
            <img src="{{asset('gambar/'.$topGame->image_game) }}" alt="PasFoto" onclick="showGame({{$topGame->id}})">
        </div>
        <div class="container-nama-harga">
            <h5>{{$topGame->game_name}}</h5>
            <p>{{$topGame->difficulty_game}}</p>
        </div>  
    </div>
    @endforeach
    @else
    <div class="container">
        <p>No data found!</p>
    </div>
    @endif
</div>
<!-------------------------- END::TOP GAMES -------------------------->

<!-------------------------- BEGIN::ALL COLLECTION -------------------------->
<div class="event-game">
    <div class="filter-promo-style">
        <h2>Game</h2>  
        <div class="filter-promo">
            <div class="span-filter" id="ShowFilter" onclick="FilterShowGame()"><i class="fas fa-filter"></i>Filter</div>
        </div>
    </div>

    <div id="componentFilterGame">
    @if(count($games))
    @foreach($games as $game)
    <div class="container-event-game">
        <div class="pas-foto">
            <img src="{{asset('gambar/'.$game->image_game) }}" alt="PasFoto" onclick="showGamex({{$game->id}})">
        </div>

        <div class="container-nama-harga">
            <h5>{{$game->game_name}}</h5>
            <p>{{$game->difficulty_game}}</p>
        </div>  
    </div>
    @endforeach
    @else
        <div class="container">
            <p>No data found!</p>
        </div>
    @endif
    </div>
</div>
<!-------------------------- END::ALL COLLECTION -------------------------->
