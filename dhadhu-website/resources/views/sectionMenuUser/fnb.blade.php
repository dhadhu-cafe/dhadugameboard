@foreach($categoryMenus as $categoryMenu)
    <input id="category-{{$categoryMenu->id}}" type="hidden" value="{{$categoryMenu->category_name}}">
    <h2>{{$categoryMenu->category_name}}</h2>
        <div class="sub-eventx">
            @if(count($menus))
            @foreach($menus as $menu)
                @if($categoryMenu->category_name == $menu->type_fnb)
                <div class="container-event">
                    <div class="pas-foto"  onclick="showFNB({{$menu->id}})">
                        <img src="{{asset('gambar/'.$menu->image_fnb) }}" alt="PasFoto">
                    </div>
                    <div class="container-nama-harga">
                        <h5>{{$menu->name_fnb}}</h5>
                        <p>{{$menu->price_fnb}}</p>
                    </div>  
                </div>
                @endif
            @endforeach
            @else
                <p>No Data Found!</p>
            @endif
        </div>
        <i class="fas fa-angle-left leftArrow"></i>
        <i class="fas fa-angle-right rightArrow"></i>
@endforeach