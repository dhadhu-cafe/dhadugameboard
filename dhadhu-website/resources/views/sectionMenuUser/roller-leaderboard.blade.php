<div class="searchLead">
    <datalist id="suggestionsGame">
    @foreach($games as $game)
    <option value="{{$game->game_name}}"></option>
    @endforeach
    </datalist>
    <input placeholder="search.." onchange="filterLeaderboard()" id="FilterLeaderboardGame" name="filterLeaderboard" class="filterLeaderboard" autoComplete="on" list="suggestionsGame"/> 
</div>

<h2 style="text-align: center">Leaderboard</h2>

<center><img src="http://i.stack.imgur.com/FhHRx.gif" alt="" id="loading_image" style="display: none; top: 200px; left: 50%; position: absolute;"></center>
<div class="container-challenge-navbar">
    <div class="challenge-navbar-menu" id="myDivLead">
        <div class="nav-play-lead hidup">High Score</div> 
        <div class="nav-play-lead">Total Score</div> 
        <div class="nav-play-lead">Time Trial</div> 
    </div>
</div>

<!-- High Score -->
<div class="play-leaderboard active"  >
    <div class="main-container-leaderboard">
    <table id="dataLeaderboardHighScore">
        <tr>
        <th>Rank</th>
        <th>Game Name</th>
        <th>Player Name</th>
        <th>Player Level</th>
        <th>High Score</th>
        </tr>

        <tbody class="ldHighScore">
        <?php $no = 0;?>
        @foreach($highScoreLeaderboards as $highScoreLeaderboard)
        <?php $no++;?>
        <tr>
        <td>{{$no}}</td>
        <td>{{$highScoreLeaderboard->game_name}}</td>
        <td>{{$highScoreLeaderboard->player_name}}</td>
        <td>{{$highScoreLeaderboard->exp}}</td>
        <td>{{$highScoreLeaderboard->score}}</td>
        </tr>  
        @endforeach
        </tbody>
    </table>
    </div>
</div>

<!-- Total Score -->
<div class="play-leaderboard"  >
    <div class="main-container-leaderboard">
    <table id="dataLeaderboard">
        <tr>
        <th>Rank</th>
        <th>Game Name</th>
        <th>Player Name</th>
        <th>Player Level</th>
        <th>Total Score</th>
        </tr>

        <tbody class="ldScore">
        <?php $no = 0;?>
        @foreach($leaderboards as $leaderboard)
        <?php $no++;?>
        <tr>
        <td>{{$no}}</td>
        <td>{{$leaderboard->game_name}}</td>
        <td>{{$leaderboard->player_name}}</td>
        <td>{{$leaderboard->exp}}</td>
        <td>{{$leaderboard->total_score}}</td>
        </tr>  
        @endforeach
        </tbody>
    </table>
    </div>
</div>

<!-- Time Trial -->
<div class="play-leaderboard" >
    <div class="main-container-leaderboard">
    <table id="dataLeaderboardTime">
        <tr>
        <th>Rank</th>
        <th>Game Name</th>
        <th>Player Name</th>
        <th>Player Level</th>
        <th>Total Time</th>
        </tr>
        <tbody class="ldTime">
        <?php $no = 0;?>
        @foreach($leaderboardsTime as $leaderboardTime)
        <?php $no++;?>
        <tr>
        <td>{{$no}}</td>
        <td>{{$leaderboardTime->game_name}}</td>
        <td>{{$leaderboardTime->player_name}}</td>
        <td>{{$leaderboardTime->exp}}</td>
        <td>{{$leaderboardTime->playtime}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>