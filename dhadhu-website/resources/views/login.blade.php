<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhadhu Game Board Cafe</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" type="text/css" crossorigin="">
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
    <link href="{{asset('assets/css/style-no5-login.css')}}" rel="stylesheet" type="text/css">
</head>
<body>

    <main>
        <div class="main-container">
            <h1>Login</h1>
            <!-- alert error -->
            @if(session()->has('success'))
                <div class="alert alert-success" style="color: green;font-weight:bold">
                    {{ session()->get('success') }}
                </div>
            @endif
            <!-- END::Alert error -->
            <!-- alert error -->
            @if(session()->has('error'))
                <div class="alert alert-danger" style="color: red;font-weight:bold">
                    {{ session()->get('error') }}
                </div>
            @endif
            <!-- END::Alert error -->
            <form method="GET" action="login/user">
                @csrf
                <div class="container-login">
                    <p>Username</p>
                        <input type="email" name="email" placeholder="Type Your Email">
                    <p>Password</p>
                        <input type="password" name="password" id="pw_user" placeholder="Type Your Password">
                        <a href="/register"><span class="forgot-password side-left">Daftar Disini</span></a>
                        <!-- <span class="forgot-password">Forgot Password</span> -->
                        <input type="submit" value="LOGIN">
                </div>
            </form>
        </div>
    </main>
    
</body>
</html>