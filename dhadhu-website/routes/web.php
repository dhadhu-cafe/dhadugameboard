<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\LoginController;
// use App\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route ::group(['middleware' => 'CekLoginMiddleware'],function(){
//BEGIN::CEK ROOM
Route::get('/dashboard/{id}/cek-room','MainController@cekRoom')->name('web.dhadhu');
//END::CEK ROOM
//BEGIN::JOIN ROOM
Route::delete('/dashboard/leave-room/{id}','MainController@leaveRoom')->name('web.dhadhu');
Route::delete('/dashboard/destroy-room/{id}','MainController@destroyRoom')->name('web.dhadhu');
Route::post('/dashboard/{id}/joinRoom','MainController@joinRoom')->name('web.dhadhu');
//END::JOIN ROOM

Route::post('/dashboard/{id}/createRoom','MainController@createRoom')->name('web.dhadhu');
Route::get('/dashboard/filter','MainController@filter')->name('web.dhadhu');
// Route::get('/dashboard/filterEvents','MainController@filterEvents')->name('web.dhadhu');
Route::get('/dashboard/multiFilterEvent','MainController@filterEvents')->name('web.dhadhu');
Route::get('/dashboard/multiFilterGame','MainController@filterGames')->name('web.dhadhu');
Route::get('/dashboard/{id}','MainController@menu')->name('web.dhadhu')->middleware('CekLoginMiddleware');
Route::get('/logout_user','LoginController@logout')->name('logoutuser');
});
Route::get('/dashboard/guest/multiFilterGame','GuestController@filterGames')->name('web.dhadhu');
Route::get('/dashboard/guest/multiFilterEvent','GuestController@filterEvents')->name('web.dhadhu');
Route::get('/dashboard','GuestController@guest')->name('web.dhadhu');

//REGISTER
Route::post('/register/post','RegisterPlayerController@store')->name('web.dhadhu');
Route::get('/register','RegisterPlayerController@index')->name('web.dhadhu');
//END::REGISTER

//LOGIN USER
Route::get('/login/user','LoginController@loginUser')->name('web.dhadhu');
Route::get('/login','LoginController@index')->name('login');

//END::LOGIN USER

//LOGIN Admin
Route::get('/login/admin','AdminController@loginAdmin')->name('web.dhadhu');
Route::get('/loginadmin','AdminController@index')->name('web.dhadhu');
//END::LOGIN Admin
Route::group(['middleware'=>'LoginAdminMiddleware'],function(){
    // group middleware admin
Route::get('/admin/{id}', 'AdminController@tampilan')->name('web.dhadhu');
//END::LOGIN Admin

// info
Route::get('/admin/info','InfoController@index')->name('indexInfo');
Route::post('/admin/editinfo/','AdminController@updateInfo');
//end info

// Route::get('/admin/{id}', function() {return view('adminpage');});
//SCORE
Route::post('/page_score/post','ScoreController@store')->name('web.dhadhu');
Route::delete('/page_score/delete/{id}','ScoreController@destroy')->name('web.dhadhu');
Route::post('/page_score/{id}/edit', 'ScoreController@update');
Route::get('/page_score/{id}','ScoreController@index')->name('web.dhadhu');
//END::SCORE

//events
Route::post('page_postingan/post','EventsController@store')->name('posts.store');
Route::post('page_postingan/edit/{id}','EventsController@update')->name('posts.store');
Route::delete('page_postingan/delete/{id}','EventsController@destroy')->name('posts.delete');
Route::get('/page_postingan/{id}','EventsController@index');
//end events

// challenge
Route::post('/page_challenge/post','ChallengesController@store');
Route::delete('/page_challenge/delete/{id}','ChallengesController@destroy');
Route::post('/page_challenge/edit/{id}', 'ChallengesController@update');
Route::get('/page_challenge/{id}','ChallengesController@index');
//END::Challenge

//challenge record
Route::post('/page_challenge_record/post','Challenge_RecordController@store');
Route::delete('/page_challenge_record/delete/{id}','Challenge_RecordController@destroy');
Route::post('/page_challenge_record/edit/{id}', 'Challenge_RecordController@update');
Route::get('/page_challenge_record/{id}','Challenge_RecordController@index');
//emd challenge record

//BEGIN::GAMES
Route::post('/page_game/post','GameController@store')->name('web.dhadhu');
Route::post('/page_game/edit/{id}','GameController@update')->name('web.dhadhu');
Route::delete('/page_game/delete/{id}','GameController@destroy')->name('web.dhadhu');
Route::get('/page_game/{id}','GameController@index')->name('web.dhadhu');
//END::GAMES

//BEGIN::FNB
Route::post('/page_fnb/post','FnbController@store')->name('web.dhadhu');
Route::post('/page_fnb/edit/{id}','FnbController@update')->name('web.dhadhu');
Route::delete('/page_fnb/delete/{id}','FnbController@destroy')->name('web.dhadhu');
Route::get('/page_fnb/{id}','FnbController@index')->name('web.dhadhu');
//END::FNB

//BEGIN:: CATEGORIES
Route::get('/page_categories/{id}','CategoriesController@index')->name('web.dhadhu');
Route::post('/page_categories/post','CategoriesController@store')->name('web.dhadhu');
Route::delete('/page_categories/delete/{id}','CategoriesController@destroy');
Route::post('/page_categories/edit/{id}', 'CategoriesController@update');
//END:::CATEGORIES



//BEGIN:: PLAYER ADMIN
Route::get('/page_player/{id}','PlayerController@index')->name('web.dhadhu');
Route::delete('/page_player/delete/{id}','PlayerController@destroy');
Route::post('/page_player/edit/{id}', 'PlayerController@update');
//END:: PLAYER ADMIN



//BEGIN:PAGE ROOM
Route::get('/page_room/{id}','RoomController@index');
Route::post('/page_room/post','RoomController@store');

Route::get('/logout','AdminController@logoutAdmin')->name('logoutadmin');
// end middleware admin
});
//cekplayer
Route::get('/list_player/{id}/{roomId}','RoomController@listPlayer');
//cekplayer

//END:ROOM
